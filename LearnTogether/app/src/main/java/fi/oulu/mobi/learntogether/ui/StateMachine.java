package fi.oulu.mobi.learntogether.ui;

/**
 * Created by raniq on 4/23/2017.
 */

public class StateMachine {
    public static enum States {CHAT_SCREEN, HOME, BUDDIES, GROUPS, SETTINGS, ABOUTUS, COURSES, GROUP_DETAIL, PERSONAL_GROUPS, COURSE_GROUPS, UNREAD_MESSAGES, BUDDY_REQUESTS, UPCOMING_MEETINGS, MEETING_SCREEN, ADD_GROUP, ADD_MEETING, COURSE_DETAIL, MEETING_DETAIL, MEETING_LOCATION}

    ;
    public static States currentState = States.HOME;

    public static States getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(States currentState) {
        StateMachine.currentState = currentState;
    }
}
