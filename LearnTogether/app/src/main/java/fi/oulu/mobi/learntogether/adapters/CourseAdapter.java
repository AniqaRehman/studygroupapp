package fi.oulu.mobi.learntogether.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.BuddyRequest;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.ui.MainActivity;
import fi.oulu.mobi.learntogether.ui.MyCourses;

/**
 * Created by raniq on 4/22/2017.
 */

public class CourseAdapter extends CursorAdapter {
    private static ProgressDialog progress;

    public CourseAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.course_group_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView nameText = (TextView) view.findViewById(R.id.courseGroupName);
        final String name = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_NAME));
        nameText.setText(name);
        TextView codeText = (TextView) view.findViewById(R.id.courseGroupCode);
        final String code = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.COURSE_CODE));
        codeText.setText(code);

        ImageButton detailButton = (ImageButton) view.findViewById(R.id.courseDetails);
        ImageButton chatButton = (ImageButton) view.findViewById(R.id.courseChat);

        detailButton.setVisibility(View.INVISIBLE);
        chatButton.setVisibility(View.INVISIBLE);

        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.deleteCourse);
        deleteButton.setOnClickListener(new CourseAdapter.MyOnClickListener(context,
                cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_SERVER_ID)),
                cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_ARN))));

    }

    private static class MyOnClickListener implements View.OnClickListener {
        Context mContext;
        String mGroupId;
        String mGroupArn;
        static LearnTogetherContract learnTogetherContract;


        public MyOnClickListener(Context context, String groupId, String groupArn) {
            mContext = context;
            mGroupId = groupId;
            mGroupArn = groupArn;
            learnTogetherContract = new LearnTogetherContract(context);
        }

        @Override
        public void onClick(final View v) {
            if (mContext instanceof MainActivity) {
                progress = progress.show(mContext, "My Courses", "Unsubscribing Course...");
                Map<String, String> messageMap = new HashMap<>();
                messageMap.put(Constants.REFRESH_MEMBERS, "" + true);
                messageMap.put(LearnTogetherContract.GroupMemberEntry.GROUP_ID, mGroupId);

                Gson gson = new Gson();
                final String message = gson.toJson(messageMap);

                v.setEnabled(false);

                final FirebaseDatabaseManager fbDb = new FirebaseDatabaseManager();
                fbDb.getGroupMembers(mGroupId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            if (postSnapshot.getValue() != null) {
                                GroupMember groupMember = postSnapshot.getValue(GroupMember.class);
                                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                                if (groupMember.getUserId().equals(preferences.getString("userId", ""))) {
                                    postSnapshot.getRef().removeValue();
                                    sendMessage(message, v, learnTogetherContract.getSubscriptionId(preferences.getString("userId", ""), mGroupId), mGroupArn, mContext, mGroupId);
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        progress.dismiss();
                    }
                });
            }
        }

        private static void sendMessage(final String messagePacket, final View v, final String subscriptionId, final String mGroupArn, final Context context, final String mGroupId) {
            new AsyncTask<Object, Object, Object>() {
                protected Object doInBackground(Object... params) {
                    try {
                        AWSSNSWrapper sns = new AWSSNSWrapper(context);
                        sns.unSubscribeMember(subscriptionId);
                        sns.publish(mGroupArn, messagePacket);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                }

                @Override
                protected void onPostExecute(Object o) {
                    boolean result = (boolean) o;
                    progress.dismiss();
                    v.setEnabled(true);
                    if (result) {
                        learnTogetherContract.clearGroupMembers(mGroupId);
                        learnTogetherContract.deleteGroup(mGroupId);
                    }
                    MyCourses.refreshList();
                }
            }.execute();
        }
    }
}