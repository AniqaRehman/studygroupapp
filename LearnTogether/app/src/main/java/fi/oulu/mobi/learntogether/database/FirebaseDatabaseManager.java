package fi.oulu.mobi.learntogether.database;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import fi.oulu.mobi.learntogether.helpers.Group;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.helpers.UserProfile;

/**
 * Created by raniq on 4/1/2017.
 */

public class FirebaseDatabaseManager {
    private FirebaseDatabase fbDB;

    public FirebaseDatabaseManager() {
        // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        fbDB = FirebaseDatabase.getInstance();
    }

    public String writeUserProfile(UserProfile userProfile) {
        DatabaseReference mDatabase = fbDB.getReference("users");
        // Creating new user node, which returns the unique key value
        // new user node would be /users/$userid/
        String userId = mDatabase.push().getKey();
        userProfile.setUserId(userId);
        // pushing user to 'users' node using the userId
        mDatabase.child(userId).setValue(userProfile);

        return userId;
    }

    public String updateUserProfile(UserProfile userProfile, String userId) {
        DatabaseReference mDatabase = fbDB.getReference("users");
        userProfile.setUserId(userId);
        // pushing user to 'users' node using the userId
        mDatabase.child(userProfile.getUserId()).setValue(userProfile);
        return userId;
    }

    public Query getGroup(String code) {
        Group newGroup = new Group();
        DatabaseReference mDatabase = fbDB.getReference("groups");
        Query query = mDatabase.child(code);
        return query;
    }

    public Query getGroupMembers(String code) {
        DatabaseReference mDatabase = fbDB.getReference("groupMembers");
        Query mQuery = mDatabase.orderByChild("groupId").equalTo("" + code);
        return mQuery;
    }

    public String saveGroup(Group newGroup) {
        DatabaseReference mDatabase = fbDB.getReference("groups");

        // pushing group to 'groups' node using the userId
        mDatabase.child(newGroup.getCourseCode()).setValue(newGroup);

        return newGroup.getCourseCode();
    }

    public void saveGroupMember(GroupMember groupMember) {
        DatabaseReference mDatabase = fbDB.getReference("groupMembers");
        // Creating new member node, which returns the unique key value
        String memberId = mDatabase.push().getKey();
        // pushing member to 'groupMembers' node using the userId
        mDatabase.child(memberId).setValue(groupMember);

    }

    public Query getUser(String userId) {
        DatabaseReference mDatabase = fbDB.getReference("users");
        Query query = mDatabase.child(userId);
        return query;
    }
}
