package fi.oulu.mobi.learntogether.helpers;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.database.LearnTogetherContract;

/**
 * Created by raniq on 4/1/2017.
 */

public class MeetingNote {

    int id;
    String serverNoteId;//userid_servermeetingid_timestamp
    String serverMeetingId;
    String userId;
    String noteType;
    String note;
    String deliveryStatus;
    String remoteLocation;
    String description;
    long timestamp;
    Bitmap imageBitMap;

    public MeetingNote() {

    }

    public MeetingNote(int id, String serverNoteId, String serverMeetingId, String userId, String noteType, String note, String status, String remoteLocation, String description, long timestamp) {
        this.id = id;
        this.serverNoteId = serverNoteId;
        this.serverMeetingId = serverMeetingId;
        this.userId = userId;
        this.noteType = noteType;
        this.note = note;
        this.deliveryStatus = status;
        this.remoteLocation = remoteLocation;
        this.description = description;
        this.timestamp = timestamp;
    }

    public MeetingNote(Map<String, String> data) {
        this.serverNoteId = data.get(LearnTogetherContract.MeetingNotes.SERVER_NOTE_ID);
        this.serverMeetingId = data.get(LearnTogetherContract.MeetingNotes.SERVER_MEETING_ID);
        this.userId = data.get(LearnTogetherContract.MeetingNotes.USER_ID);
        this.noteType = data.get(LearnTogetherContract.MeetingNotes.NOTE_TYPE);
        this.note = data.get(LearnTogetherContract.MeetingNotes.NOTE);
        this.deliveryStatus = data.get(LearnTogetherContract.MeetingNotes.STATUS);
        this.remoteLocation = data.get(LearnTogetherContract.MeetingNotes.REMOTE_LOCATION);
        this.description = data.get(LearnTogetherContract.MeetingNotes.DESCRIPTION);
        this.timestamp = Long.parseLong(data.get(LearnTogetherContract.MeetingNotes.TIMESTAMP));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerMeetingId() {
        return serverMeetingId;
    }

    public void setServerMeetingId(String serverMeetingId) {
        this.serverMeetingId = serverMeetingId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRemoteLocation() {
        return remoteLocation;
    }

    public void setRemoteLocation(String remoteLocation) {
        this.remoteLocation = remoteLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getServerNoteId() {
        return serverNoteId;
    }

    public void setServerNoteId(String serverNoteId) {
        this.serverNoteId = serverNoteId;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Bitmap getImageBitMap() {
        return imageBitMap;
    }

    public void setImageBitMap(Bitmap imageBitMap) {
        this.imageBitMap = imageBitMap;
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put(LearnTogetherContract.MeetingNotes.SERVER_NOTE_ID, this.serverNoteId);
        map.put(LearnTogetherContract.MeetingNotes.SERVER_MEETING_ID, this.serverMeetingId);
        map.put(LearnTogetherContract.MeetingNotes.USER_ID, this.userId);
        map.put(LearnTogetherContract.MeetingNotes.NOTE_TYPE, this.noteType);
        map.put(LearnTogetherContract.MeetingNotes.NOTE, this.note);
        map.put(LearnTogetherContract.MeetingNotes.STATUS, this.deliveryStatus);
        map.put(LearnTogetherContract.MeetingNotes.DESCRIPTION, this.description);
        map.put(LearnTogetherContract.MeetingNotes.TIMESTAMP, "" + this.timestamp);

        return map;
    }
}
