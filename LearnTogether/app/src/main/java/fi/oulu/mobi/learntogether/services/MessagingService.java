package fi.oulu.mobi.learntogether.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.CourseAdapter;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSS3Wrapper;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.BuddyRequest;
import fi.oulu.mobi.learntogether.helpers.Chat;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Group;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.helpers.Meeting;
import fi.oulu.mobi.learntogether.helpers.MeetingNote;
import fi.oulu.mobi.learntogether.ui.BuddyRequests;
import fi.oulu.mobi.learntogether.ui.ChatFragment;
import fi.oulu.mobi.learntogether.ui.CourseDetailFragment;
import fi.oulu.mobi.learntogether.ui.CourseGroupFragment;
import fi.oulu.mobi.learntogether.ui.HomeFragment;
import fi.oulu.mobi.learntogether.ui.MainActivity;
import fi.oulu.mobi.learntogether.ui.MeetingDetails;
import fi.oulu.mobi.learntogether.ui.MeetingsFragment;
import fi.oulu.mobi.learntogether.ui.MyBuddies;
import fi.oulu.mobi.learntogether.ui.MyGroupFragment;
import fi.oulu.mobi.learntogether.ui.PersonalGroupFragment;
import fi.oulu.mobi.learntogether.ui.StateMachine;
import fi.oulu.mobi.learntogether.ui.UnreadMessages;

/**
 * Created by raniq on 4/1/2017.
 */

public class MessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCMMessagingService";
    public static final String NEW_MESSAGE_RECEIVED = "New Message Received";
    public static final String NEW_BUDDY_REQUEST = "New Buddy Request";
    private static final String NEW_MEETING = "New Meeting Added";
    private static final String NEW_NOTE = "New Meeting Note Received";
    private static FirebaseDatabaseManager fbDb;
    private static LearnTogetherContract ltDb;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().containsKey("default")) {
            String receivedData = remoteMessage.getData().get("default");
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            Gson gson = new Gson();
            Map<String, String> data = new HashMap<>();
            data = gson.fromJson(receivedData, HashMap.class);
            ltDb = new LearnTogetherContract(getApplicationContext());

            if (data.containsKey(LearnTogetherContract.ChatEntry.MESSAGE)) {
                //saving data

                if (!data.get(LearnTogetherContract.ChatEntry.USER_ID).equals(preferences.getString("userId", ""))) {
                    saveIncomingMessage(data);
                    if (StateMachine.getCurrentState() != StateMachine.States.CHAT_SCREEN) { //create notification
                        if (StateMachine.getCurrentState() == StateMachine.States.UNREAD_MESSAGES) {
                            UnreadMessages.runOnUI(new Runnable() {
                                public void run() {
                                    try {
                                        UnreadMessages
                                                .refreshUnreadMessageList();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else if (StateMachine.getCurrentState() == StateMachine.States.COURSE_GROUPS) {
                            CourseGroupFragment.runOnUI(new Runnable() {
                                public void run() {
                                    try {
                                        CourseGroupFragment
                                                .refreshUnreadMessageList();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        } else {
                            if (!preferences.getBoolean(Constants.ALL_NOTIFICATION, false) && !preferences.getBoolean(Constants.CHAT_NOTIFICATION, false))
                                createNotification(data.get(LearnTogetherContract.ChatEntry.MESSAGE), NEW_MESSAGE_RECEIVED, Constants.CHAT_NOTIFICATION_ID);
                        }
                    } else {
                        ChatFragment.runOnUI(new Runnable() {
                            public void run() {
                                try {
                                    ChatFragment.positionChange = false;
                                    ChatFragment
                                            .refreshView();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }
            } else if (data.containsKey(LearnTogetherContract.BuddyRequestEntry.TABLE_NAME)) {
                saveBuddyRequest(data);
                GroupMember groupMember = new GroupMember();
                groupMember = ltDb.getGroupMember(data.get(LearnTogetherContract.BuddyRequestEntry.USER_ID));
                if (groupMember != null && groupMember.getId() != 0) {
                    if (!preferences.getBoolean(Constants.ALL_NOTIFICATION, false) && !preferences.getBoolean(Constants.BUDDY_NOTIFICATION, false))
                        createNotification(groupMember.getUserFirstName() + " " + groupMember.getUserLastName(),
                                NEW_BUDDY_REQUEST, Constants.BUDDY_NOTIFICATION_ID);
                }
                if (StateMachine.getCurrentState() == StateMachine.States.BUDDY_REQUESTS)
                    BuddyRequests.runOnUI(new Runnable() {
                        public void run() {
                            try {
                                BuddyRequests
                                        .refreshBuddyRequestList();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            } else if (data.containsKey(LearnTogetherContract.BuddyRequestEntry.ACCEPTED)) {
                updateBuddyRequest(data, true);
                getBuddyData(data.get(LearnTogetherContract.BuddyRequestEntry.USER_ID));
            } else if (data.containsKey(LearnTogetherContract.BuddyRequestEntry.REJECTED)) {
                updateBuddyRequest(data, false);
                if (StateMachine.getCurrentState() == StateMachine.States.BUDDY_REQUESTS)
                    BuddyRequests.runOnUI(new Runnable() {
                        public void run() {
                            try {
                                BuddyRequests
                                        .refreshBuddyRequestList();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }else if(data.containsKey(Constants.NFC_BUDDY_MESSAGE)){
                getBuddyData(data.get(Constants.NFC_BUDDY_MESSAGE));
            }
            else if (data.containsKey(Constants.REFRESH_MEMBERS)) {
                updateGroupMembers(data.get(LearnTogetherContract.GroupMemberEntry.GROUP_ID));
            } else if (data.containsKey(LearnTogetherContract.GroupMemberEntry.SUBSCRIPTION_ID) && data.containsKey(LearnTogetherContract.GroupMemberEntry.GROUP_ID)) {
                saveGroupData(data.get(LearnTogetherContract.GroupMemberEntry.GROUP_ID));
            } else if (data.containsKey(LearnTogetherContract.Meeting.SERVER_MEETING_ID) && data.containsKey(LearnTogetherContract.Meeting.GROUP_ID) && data.containsKey(LearnTogetherContract.Meeting.LOCATION)) {
                if (!data.get(LearnTogetherContract.Meeting.USER_ID).equals(preferences.getString("userId", ""))) {
                    saveMeetingData(data);
                    if (StateMachine.getCurrentState() != StateMachine.States.MEETING_SCREEN) {
                        if (!preferences.getBoolean(Constants.ALL_NOTIFICATION, false) && !preferences.getBoolean(Constants.MEETING_NOTIFICATION, false))
                            createNotification(data.get(LearnTogetherContract.Meeting.TITLE),
                                    NEW_MEETING, Constants.MEETING_NOTIFICATION_ID);
                    }
                }
            } else if (data.containsKey(LearnTogetherContract.MeetingNotes.SERVER_MEETING_ID) && data.containsKey(LearnTogetherContract.MeetingNotes.NOTE) && data.containsKey(LearnTogetherContract.MeetingNotes.SERVER_NOTE_ID)) {
                saveNoteData(data);
                if (StateMachine.getCurrentState() != StateMachine.States.MEETING_DETAIL) {
                    if (!preferences.getBoolean(Constants.ALL_NOTIFICATION, false) && !preferences.getBoolean(Constants.MEETING_NOTE_NOTIFICATION, false))
                        createNotification(data.get(LearnTogetherContract.MeetingNotes.NOTE),
                                NEW_NOTE, Constants.NOTE_NOTIFICATION_ID);
                }

            }
        }

    }

    private void saveNoteData(Map<String, String> data) {
        final MeetingNote meetingNote = new MeetingNote(data);
        ltDb = new LearnTogetherContract(getApplicationContext());
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (meetingNote.getNoteType().equals(Constants.NOTE_TYPE.PICTURE.name())) {
            if (!data.get(LearnTogetherContract.Meeting.USER_ID).equals(preferences.getString("userId", ""))) {
                AWSS3Wrapper s3 = new AWSS3Wrapper(getApplicationContext());
                if (s3.ifFileexists(meetingNote.getRemoteLocation())) {
                    String IMAGE_DIR_PATH = Environment.getExternalStorageDirectory()
                            + File.separator + Constants.FOLDER_NAME + "/MeetingNotes/" + meetingNote.getServerMeetingId() + "/";
                    File imageDirPath = new File(IMAGE_DIR_PATH);

                    if (!imageDirPath.exists()) {
                        imageDirPath.mkdirs();
                    }
                    File file = new File(imageDirPath, meetingNote.getServerNoteId() + ".jpg");
                    if (file.exists()) {
                        file.delete();
                    }

                    FileOutputStream out = null;
                    try {
                        InputStream object = s3.getFileFromS3(meetingNote.getRemoteLocation());
                        if (object != null) {
                            out = new FileOutputStream(file);
                            byte[] b = new byte[2048];
                            int length;
                            while ((length = object.read(b)) != -1) {
                                out.write(b, 0, length);
                            }
                            object.close();
                            out.flush();
                            out.close();
                        }

                        meetingNote.setNote(file.getAbsolutePath());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        ltDb.addMeetingNote(meetingNote);
        if (StateMachine.getCurrentState() == StateMachine.States.MEETING_DETAIL) {
            MeetingDetails.runOnUI(new Runnable() {
                public void run() {
                    try {
                        MeetingDetails
                                .refreshList(meetingNote.getServerMeetingId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void saveMeetingData(Map<String, String> data) {
        final Meeting meeting = new Meeting(data);
        ltDb = new LearnTogetherContract(getApplicationContext());
        ltDb.addMeeting(meeting);
        if (StateMachine.getCurrentState() == StateMachine.States.MEETING_SCREEN) {
            MeetingsFragment.runOnUI(new Runnable() {
                public void run() {
                    try {
                        MeetingsFragment
                                .refreshList(meeting.getGroupId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void saveGroupData(final String groupId) {
        fbDb = new FirebaseDatabaseManager();
        ltDb = new LearnTogetherContract(getApplicationContext());
        fbDb.getGroup(groupId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Group group = dataSnapshot.getValue(Group.class);
                    group.setGroupServerId(dataSnapshot.getKey());
                    ltDb.addGroup(group);
                    updateGroupMembers(groupId);
                    if (StateMachine.getCurrentState() == StateMachine.States.PERSONAL_GROUPS)
                        PersonalGroupFragment.runOnUI(new Runnable() {
                            public void run() {
                                try {
                                    PersonalGroupFragment
                                            .refreshGroupList();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // read query is cancelled.
            }
        });

    }

    private void getBuddyData(String userId) {
        fbDb = new FirebaseDatabaseManager();
        ltDb = new LearnTogetherContract(getApplicationContext());
        fbDb.getUser(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Buddy buddy = dataSnapshot.getValue(Buddy.class);
                            buddy.setUserId(dataSnapshot.getKey());

                            saveProfilePicture(buddy, getApplicationContext());

                            ltDb.addBuddies(buddy);
                            if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                                MyBuddies.runOnUI(new Runnable() {
                                    public void run() {
                                        try {
                                            MyBuddies
                                                    .refreshBuddyList();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // read query is cancelled.
                    }
                });

    }

    private String saveProfilePicture(final Buddy buddy, final Context context) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                AWSS3Wrapper s3 = new AWSS3Wrapper(context);
                if (s3.ifFileexists(buddy.getProfilePicture())) {
                    String IMAGE_DIR_PATH = Environment.getExternalStorageDirectory()
                            + File.separator + Constants.FOLDER_NAME + File.separator + Constants.PROFILE_FOLDER_NAME
                            + File.separator + buddy.getUserId() + "/";
                    File imageDirPath = new File(IMAGE_DIR_PATH);

                    if (!imageDirPath.exists()) {
                        imageDirPath.mkdirs();
                    }
                    File file = new File(imageDirPath, Constants.PI_NAME);
                    if (file.exists()) {
                        file.delete();
                    }

                    FileOutputStream out = null;
                    try {
                        InputStream object = s3.getFileFromS3(buddy.getProfilePicture());
                        if (object != null) {
                            out = new FileOutputStream(file);
                            byte[] b = new byte[2048];
                            int length;
                            while ((length = object.read(b)) != -1) {
                                out.write(b, 0, length);
                            }
                            object.close();
                            out.flush();
                            out.close();
                        }
                        return file.getAbsolutePath();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        return "";
                    } catch (IOException e) {
                        e.printStackTrace();
                        return "";
                    }
                }
                return "";
            }

            @Override
            protected void onPostExecute(Object o) {
                String result = (String) o;
                if (!result.isEmpty()) {
                    LearnTogetherContract learnTogetherContract = new LearnTogetherContract(context);
                    learnTogetherContract.updateBuddyProfile(buddy.getUserId(), result);
                    if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                        MyBuddies.runOnUI(new Runnable() {
                            public void run() {
                                try {
                                    MyBuddies
                                            .refreshBuddyList();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                } else {

                }
            }
        }.execute();
        return "";
    }

    private void updateBuddyRequest(Map<String, String> data, boolean accepted) {
        LearnTogetherContract ltDb = new LearnTogetherContract(getApplicationContext());
        if (accepted)
            ltDb.updateBuddyStatus(data.get(LearnTogetherContract.BuddyRequestEntry.USER_ID), Constants.BUDDY_REQUEST_STATUS.ACCEPTED.name());
        else
            ltDb.updateBuddyStatus(data.get(LearnTogetherContract.BuddyRequestEntry.USER_ID), Constants.BUDDY_REQUEST_STATUS.REJECTED.name());
    }

    private void saveBuddyRequest(Map<String, String> data) {
        LearnTogetherContract ltDb = new LearnTogetherContract(getApplicationContext());
        BuddyRequest buddyRequest = new BuddyRequest();
        buddyRequest.setTimestamp(Long.parseLong(data.get(LearnTogetherContract.BuddyRequestEntry.TIMESTAMP)));
        buddyRequest.setStatus(Constants.BUDDY_REQUEST_STATUS.RECEIVED.name());
        buddyRequest.setUserId(data.get(LearnTogetherContract.BuddyRequestEntry.USER_ID));
        ltDb.addBuddyRequest(buddyRequest);
    }

    private void saveIncomingMessage(Map<String, String> data) {
        LearnTogetherContract ltDb = new LearnTogetherContract(getApplicationContext());
        Chat chat = new Chat();
        chat.setMessage(data.get(LearnTogetherContract.ChatEntry.MESSAGE));
        chat.setMessageType(data.get(LearnTogetherContract.ChatEntry.MESSAGE_TYPE));
        chat.setTimestamp(Long.parseLong(data.get(LearnTogetherContract.ChatEntry.TIMESTAMP)));
        chat.setServerMessageId(data.get(LearnTogetherContract.ChatEntry.SERVER_MESSAGE_ID));
        chat.setMessageStatus(Constants.MESSAGE_STATUS.UNREAD.name());
        chat.setGroupId(data.get(LearnTogetherContract.ChatEntry.GROUP_ID));
        chat.setUserId(data.get(LearnTogetherContract.ChatEntry.USER_ID));
        ltDb.saveChat(chat);
    }

    public void updateGroupMembers(final String groupId) {
        fbDb = new FirebaseDatabaseManager();
        ltDb = new LearnTogetherContract(getApplicationContext());

        fbDb.getGroupMembers(groupId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ltDb.clearGroupMembers(groupId);
                Log.e("Count For value event ", "" + dataSnapshot.getChildrenCount());
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if (postSnapshot.getValue() != null) {
                        GroupMember groupMember = postSnapshot.getValue(GroupMember.class);
                        ltDb.saveGroupMember(groupMember);

                        fbDb.getUser(groupMember.getUserId()).addListenerForSingleValueEvent(
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue() != null) {
                                            Buddy buddy = dataSnapshot.getValue(Buddy.class);
                                            buddy.setUserId(dataSnapshot.getKey());
                                            ltDb.updateGroupMember(buddy);
                                            if (StateMachine.getCurrentState() == StateMachine.States.GROUP_DETAIL) {
                                                CourseDetailFragment.runOnUI(new Runnable() {
                                                    public void run() {
                                                        try {
                                                            CourseDetailFragment
                                                                    .refreshMembersList();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        // read query is cancelled.
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void createNotification(String notification, String notificationHeading, int notificationId) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(notificationHeading)
                .setContentText(notification)
                .setAutoCancel(true)
                .setSound(notificationSoundURI)
                .setContentIntent(resultIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, mNotificationBuilder.build());
    }

    private static String createMessagePacket(Chat newChat) {
        String message = "";
        Map<String, String> messageMap = new HashMap<>();
        messageMap.put(LearnTogetherContract.ChatEntry.GROUP_ID, newChat.getGroupId());
        messageMap.put(LearnTogetherContract.ChatEntry.TIMESTAMP, "" + newChat.getTimestamp());
        messageMap.put(LearnTogetherContract.ChatEntry.SERVER_MESSAGE_ID, newChat.getServerMessageId());
        messageMap.put(LearnTogetherContract.ChatEntry.MESSAGE, newChat.getMessage());
        messageMap.put(LearnTogetherContract.ChatEntry.MESSAGE_TYPE, newChat.getMessageType());
        messageMap.put(LearnTogetherContract.ChatEntry.USER_ID, newChat.getUserId());
        Gson gson = new Gson();
        message = gson.toJson(messageMap);

        return message;
    }

    public static void resendMessages(final Context context) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                try {
                    ltDb = new LearnTogetherContract(context);
                    List<Chat> chats = ltDb.getPendingMessages();
                    for (Chat oneChat : chats) {
                        String message = createMessagePacket(oneChat);
                        AWSSNSWrapper sns = new AWSSNSWrapper(context);
                        Group group = ltDb.getGroup(oneChat.getGroupId());
                        sns.publish(group.getGroupARN(), message);
                        ltDb.updateMessageStatus(oneChat.getServerMessageId(), Constants.MESSAGE_STATUS.DELIVERED.name());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "";
            }
        }.execute();
    }
}

