package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.ui.MeetingDetails;
import fi.oulu.mobi.learntogether.ui.MeetingLocation;
import fi.oulu.mobi.learntogether.ui.MeetingsFragment;

/**
 * Created by Sam on 4/21/2017.
 */

public class MeetingsListAdapter extends CursorAdapter{
    LearnTogetherContract learnTogetherContract;
    private Context mContext;
    private Boolean isPrevious;

    public MeetingsListAdapter(Context context, Cursor c, boolean autoRequery, Boolean previous) {
        super(context, c, autoRequery);
        mContext = context;
        isPrevious = previous;
        learnTogetherContract = new LearnTogetherContract(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View listLineView = LayoutInflater.from(context).inflate(R.layout.list_item_meeting, parent, false);
        return listLineView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        final String location= cursor.getString(4);
        final long meetingId = cursor.getLong(0);

        TextView meetingTitle = (TextView) view.findViewById(R.id.meeting_title);
        meetingTitle.setText(cursor.getString(5));
        meetingTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMeetingDetails(meetingId);
            }
        });
        TextView meetingDatetime = (TextView) view.findViewById(R.id.meeting_date_time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(cursor.getLong(7));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String result = dateFormat.format(calendar.getTime());
        meetingDatetime.setText(result);
        meetingDatetime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMeetingDetails(meetingId);
            }
        });

        TextView meetingPlace = (TextView) view.findViewById(R.id.meeting_place);
        String[] locationStrings = cursor.getString(4).split("_");
        meetingPlace.setText(locationStrings[0]);
        Button locationButton = (Button) view.findViewById(R.id.button_show_location);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMeetingLocation(location);
            }
        });
    }

    private void showMeetingLocation(String place) {
        Intent mapActivityIntent = new Intent(mContext, MeetingLocation.class);
        mapActivityIntent.putExtra(MeetingLocation.MAP_FOR, MeetingsFragment.MEETING_LOCATION_FOR_EXTRA);
        mapActivityIntent.putExtra(MeetingsFragment.MEETING_LOCATION_EXTRA, place);
        mContext.startActivity(mapActivityIntent);
    }

    private void launchMeetingDetails(long meetingId) {
        Intent meetingDeets = new Intent(mContext, MeetingDetails.class);
        meetingDeets.putExtra(Constants.MEETING_ID_EXTRA_KEY, meetingId);
        meetingDeets.putExtra(Constants.MEETING_IS_PREVIOUS_EXTRA, isPrevious);
        mContext.startActivity(meetingDeets);
    }


}
