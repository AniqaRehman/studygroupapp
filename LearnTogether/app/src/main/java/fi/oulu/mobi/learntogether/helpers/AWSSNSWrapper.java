package fi.oulu.mobi.learntogether.helpers;

import android.content.Context;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.CreatePlatformEndpointResult;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by raniq on 4/19/2017.
 */

public class AWSSNSWrapper {

    private static AmazonSNS snsClient = null;
    private static String applicationName;
    private static String Platform;
    private static String applicationARN;

    public AWSSNSWrapper(Context context) {

        if (snsClient == null) {
            CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                    context,
                    Constants.COGNITO_ACOUNT_ID,
                    Constants.COGNITO_POOL_ID,
                    Constants.COGNITO_ROLE_ARN,
                    Constants.COGNITO_ROLE_ARN,
                    Regions.US_WEST_2);

            snsClient = new AmazonSNSClient(cognitoProvider);
            applicationName = "LearnTogether";
            Platform = "GCM";
            applicationARN = "arn:aws:sns:us-west-2:347118411249:app/GCM/LearnTogether";
            snsClient.setRegion(Region.getRegion(Regions.US_WEST_2));
        }
    }

    public static enum Platform {
        // Apple Push Notification Service
        APNS,
        // Sandbox version of Apple Push Notification Service
        APNS_SANDBOX,
        // Amazon Device Messaging
        ADM,
        // Google Cloud Messaging
        GCM,
        // Baidu CloudMessaging Service
        BAIDU,
        // Windows Notification Service
        WNS,
        // Microsoft Push Notificaion Service
        MPNS;
    }


    public String getARNFromRegID(String name, String RegID) {
        String ARN = "";
        try {
            CreatePlatformEndpointRequest platformEndpointRequest = new CreatePlatformEndpointRequest();
            platformEndpointRequest.setCustomUserData(name);
            platformEndpointRequest.setToken(RegID);
            Map<String, String> attribute = new HashMap<String, String>();
            attribute.put("Enabled", "true");
            platformEndpointRequest.setAttributes(attribute);
            platformEndpointRequest.setPlatformApplicationArn(applicationARN);
            CreatePlatformEndpointResult result = snsClient
                    .createPlatformEndpoint(platformEndpointRequest);
            ARN = result.getEndpointArn();

        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon SNS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with SNS, such as not "
                            + "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            ace.printStackTrace();
        }
        return ARN;
    }

    public String reRegisterWithSNS(String arn, String GCMRegId, String name) {
        try {
            Map<String, String> attribs = new HashMap<String, String>();
            attribs.put("Token", GCMRegId);
            attribs.put("Enabled", "true");
            SetEndpointAttributesRequest saeReq = new SetEndpointAttributesRequest()
                    .withEndpointArn(arn).withAttributes(attribs);
            snsClient.setEndpointAttributes(saeReq);
        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon SNS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with SNS, such as not "
                            + "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());

            ace.printStackTrace();
        }
        return arn;
    }

    public PublishResult publish(String endpointArn, String message) throws AmazonClientException, AmazonServiceException {

        try {

            PublishRequest publishRequest = new PublishRequest();
            publishRequest.setMessageStructure("http");
            publishRequest.setTargetArn(endpointArn);
            // Display the message that will be sent to the endpoint/
            System.err.println(message);
            publishRequest.setMessage(message);
            PublishResult result = snsClient.publish(publishRequest);

            return result;
        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon SNS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

            ase.printStackTrace();
            throw ase;
        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with SNS, such as not "
                            + "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            ace.printStackTrace();
            throw ace;
        }
    }

    public String subscribeToTopic(String arn, String topicArn) throws AmazonClientException, AmazonServiceException {
        SubscribeResult subResult = null;
        try {
            SubscribeRequest subRequest = new SubscribeRequest(topicArn,
                    "application", arn);

            subResult = snsClient.subscribe(subRequest);
        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon SNS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

            ase.printStackTrace();
            throw ase;
        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with SNS, such as not "
                            + "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            ace.printStackTrace();
            throw ace;
        }

        return subResult.getSubscriptionArn();
    }

    public String createTopic(String Name) throws AmazonClientException, AmazonServiceException {
        String topicArn = null;
        try {
            Name = Name.replace("\\s", "");
            Name = Name.replace(" ", "");

            CreateTopicRequest createTopicRequest = new CreateTopicRequest(Name);
            CreateTopicResult createTopicResult = snsClient
                    .createTopic(createTopicRequest);

            topicArn = createTopicResult.getTopicArn();
        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon SNS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

            ase.printStackTrace();
            throw ase;
        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with SNS, such as not "
                            + "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            ace.printStackTrace();
            throw ace;
        }
        return topicArn;

    }

    public boolean unSubscribeMember(String subscriptionArn) {
        // TODO Auto-generated method stub
        boolean result = false;
        try {
            snsClient.unsubscribe(subscriptionArn);
            result = true;
        } catch (AmazonServiceException ase) {
            System.out
                    .println("Caught an AmazonServiceException, which means your request made it "
                            + "to Amazon SNS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

            ase.printStackTrace();
            result = false;
            throw ase;
        } catch (AmazonClientException ace) {
            System.out
                    .println("Caught an AmazonClientException, which means the client encountered "
                            + "a serious internal problem while trying to communicate with SNS, such as not "
                            + "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            ace.printStackTrace();

            result = false;
            throw ace;
        }

        return result;
    }

    public long getTime() {
        return System.currentTimeMillis();
    }

    public static String jsonify(Object message) {
        try {
            Gson gson = new Gson();
            return gson.toJson(message);
        } catch (Exception e) {
            e.printStackTrace();
            throw (RuntimeException) e;
        }
    }

}
