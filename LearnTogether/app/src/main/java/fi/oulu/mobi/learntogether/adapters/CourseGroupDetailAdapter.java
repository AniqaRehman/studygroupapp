package fi.oulu.mobi.learntogether.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.BuddyRequest;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.ui.MainActivity;

/**
 * Created by raniq on 4/22/2017.
 */

public class CourseGroupDetailAdapter extends CursorAdapter {
    static LearnTogetherContract learnTogetherContract;
    static SharedPreferences preference;
    private static ProgressDialog progress;

    public CourseGroupDetailAdapter(Context context, Cursor c) {
        super(context, c);
        learnTogetherContract = new LearnTogetherContract(context);
        preference = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_course_detail, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView nameText = (TextView) view.findViewById(R.id.buddyName);
        final String name = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupMemberEntry.FIRST_NAME)) + " " + cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupMemberEntry.LAST_NAME));
        nameText.setText(name);

        Button addBuddy = (Button) view.findViewById(R.id.addBuddy);
        addBuddy.setOnClickListener(new MyOnClickListener(context, cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupMemberEntry.USER_ID)), cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupMemberEntry.USER_ARN))));
        if (learnTogetherContract.checkBuddyRequest(cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupMemberEntry.USER_ID))))
            addBuddy.setEnabled(false);
        else
            addBuddy.setEnabled(true);
    }

    private static class MyOnClickListener implements View.OnClickListener {
        Context mContext;
        String mUserId;
        String mUserArn;

        public MyOnClickListener(Context context, String userId, String userArn) {
            mContext = context;
            mUserId = userId;
            mUserArn = userArn;
        }

        @Override
        public void onClick(View v) {
            if (mContext instanceof MainActivity) {
                progress = progress.show(mContext, "Course Members", "Adding as Buddy...");
                BuddyRequest buddyRequest = new BuddyRequest();
                buddyRequest.setUserId(mUserId);
                buddyRequest.setStatus(Constants.BUDDY_REQUEST_STATUS.PENDING.name());
                buddyRequest.setTimestamp(System.currentTimeMillis());

                learnTogetherContract.addBuddyRequest(buddyRequest);
                ((Button) v.findViewById(R.id.addBuddy)).setEnabled(false);
                sendMessage(createMessagePacket(buddyRequest), mUserArn, mContext);

            }
        }

        private String createMessagePacket(BuddyRequest buddyRequest) {
            String message = "";
            Map<String, String> messageMap = new HashMap<>();
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.TABLE_NAME, "" + true);
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.USER_ID, preference.getString("userId", ""));
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.TIMESTAMP, "" + buddyRequest.getTimestamp());
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.RECEVER_ID, "" + buddyRequest.getUserId());

            Gson gson = new Gson();
            message = gson.toJson(messageMap);

            return message;
        }
    }

    private static void sendMessage(final String messagePacket, final String mUserArn, final Context context) {
        new AsyncTask<Object, Object, Object>() {
            protected Object doInBackground(Object... params) {
                try {
                    AWSSNSWrapper sns = new AWSSNSWrapper(context);

                    sns.publish(mUserArn, messagePacket);

                    Gson gson = new Gson();
                    Map<String, String> map = new HashMap<>();
                    map = (Map<String, String>) gson.fromJson(messagePacket, map.getClass());

                    learnTogetherContract.updateBuddyStatus(map.get(LearnTogetherContract.BuddyRequestEntry.RECEVER_ID), Constants.BUDDY_REQUEST_STATUS.DELIVERED.name());

                    return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                progress.dismiss();
                boolean result = (boolean) o;
            }
        }.execute();
    }
}