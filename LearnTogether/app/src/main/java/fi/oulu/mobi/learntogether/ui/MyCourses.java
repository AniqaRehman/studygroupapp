package fi.oulu.mobi.learntogether.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.CourseAdapter;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.*;

/**
 * Created by raniq on 4/1/2017.
 */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class MyCourses extends Fragment {
    FloatingActionButton addCourses;
    fi.oulu.mobi.learntogether.helpers.UserProfile userProfile;
    static LearnTogetherContract ltDb;
    FirebaseDatabaseManager fbDb;
    ProgressDialog progress;

    static Context context = null;

    private static ListView mCourseListView;
    private CourseAdapter courseAdapter;

    public MyCourses() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_courses, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == R.id.action_add_course) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View dialoglayout = inflater.inflate(R.layout.add_course, null);
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(dialoglayout);
            builder.setTitle(Constants.ADD_COURSE);
            builder.setPositiveButton("Ok", null);
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog mAlertDialog = builder.create();

            mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

                @Override
                public void onShow(DialogInterface dialog) {

                    Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    button.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            EditText name = (EditText) dialoglayout.findViewById(R.id.courseName);
                            EditText code = (EditText) dialoglayout.findViewById(R.id.courseCode);

                            if (code.getText().toString().matches("\\d+[a-zA-Z]")) {
                                Group oneGroup = new Group();
                                oneGroup.setGroupName(name.getText().toString());
                                oneGroup.setCourseCode(code.getText().toString());
                                if (!ltDb.checkCourseGroup(code.getText().toString())) {
                                    oneGroup.setGroupType(Constants.GROUP_TYPES.COURSE.name());
                                    registerGroup(oneGroup);
                                    mAlertDialog.dismiss();
                                } else {
                                    Toast.makeText(getActivity().getApplicationContext(), "Course already exists!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity().getApplicationContext(), "Kindly check the format of code 123456X", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            });

            mAlertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_courses, container, false);
        setHasOptionsMenu(false);

        addCourses = (FloatingActionButton) rootView.findViewById(R.id.addCourses);

        context = getContext();

        addCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog to add course
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialoglayout = inflater.inflate(R.layout.add_course, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialoglayout);
                builder.setTitle(Constants.ADD_COURSE);
                builder.setPositiveButton("Ok", null);
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                final AlertDialog mAlertDialog = builder.create();

                mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(DialogInterface dialog) {

                        Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                        button.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                EditText name = (EditText) dialoglayout.findViewById(R.id.courseName);
                                EditText code = (EditText) dialoglayout.findViewById(R.id.courseCode);

                                if (code.getText().toString().matches("\\d+[a-zA-Z]")) {
                                    Group oneGroup = new Group();
                                    oneGroup.setGroupName(name.getText().toString());
                                    oneGroup.setCourseCode(code.getText().toString());
                                    if (!ltDb.checkCourseGroup(code.getText().toString())) {
                                        oneGroup.setGroupType(Constants.GROUP_TYPES.COURSE.name());
                                        registerGroup(oneGroup);
                                        mAlertDialog.dismiss();
                                    } else {
                                        Toast.makeText(getActivity().getApplicationContext(), "Course already exists!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity().getApplicationContext(), "Kindly check the format of code 123456X", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });

                mAlertDialog.show();
            }
        });
        ltDb = new LearnTogetherContract(getActivity().getApplication());
        fbDb = new FirebaseDatabaseManager();

        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplication());
        userProfile = ltDb.getUserProfile(preferences.getString("userId", ""));

        courseAdapter = new CourseAdapter(getContext(), ltDb.getCourseGroups());
        mCourseListView = (ListView) rootView.findViewById(R.id.myCourses);
        mCourseListView.setAdapter(courseAdapter);

        mCourseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) courseAdapter.getItem(position);
                Fragment chatsFragment = CourseDetailFragment
                        .newInstance(cursor.getString(cursor.getColumnIndex(
                                LearnTogetherContract.GroupEntry.GROUP_SERVER_ID)),
                                Constants.GROUP_TYPES.COURSE.name());
                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, chatsFragment).addToBackStack(Constants.MY_COURSES).commit();
                getActivity().setTitle(Constants.GROUP_MEMBERS);
                StateMachine.setCurrentState(StateMachine.States.GROUP_DETAIL);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        (getActivity()).setTitle(Constants.MY_COURSES);
        StateMachine.setCurrentState(StateMachine.States.COURSES);

    }

    private void registerGroup(final Group oneGroup) {
        progress = progress.show(context, "My Courses", "Adding Course...");

        fbDb.getGroup(oneGroup.getCourseCode()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue() != null) {
                            Group group = dataSnapshot.getValue(Group.class);
                            ltDb.saveGroup(group);
                            registerExistingGroupAWS(group, getContext());
                        } else {
                            registerGroupAWS(oneGroup);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        progress.dismiss();
                        // read query is cancelled.
                    }
                });

        fbDb.getGroupMembers(oneGroup.getCourseCode()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Count For value event ", "" + dataSnapshot.getChildrenCount());
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    if (postSnapshot.getValue() != null) {
                        GroupMember groupMember = postSnapshot.getValue(GroupMember.class);
                        ltDb.saveGroupMember(groupMember);

                        fbDb.getUser(groupMember.getUserId()).addListenerForSingleValueEvent(
                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue() != null) {
                                            Buddy buddy = dataSnapshot.getValue(Buddy.class);
                                            buddy.setUserId(dataSnapshot.getKey());
                                            ltDb.updateGroupMember(buddy);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        // read query is cancelled.
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void registerGroupAWS(final Group oneGroup) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                if (userProfile != null) {
                    AWSSNSWrapper sns = new AWSSNSWrapper(getActivity().getApplicationContext());
                    String arn = sns.createTopic(oneGroup.getCourseCode());

                    Group newGroup = new Group();
                    newGroup.setCourseCode(oneGroup.getCourseCode());
                    newGroup.setGroupType(oneGroup.getGroupType());
                    newGroup.setGroupName(oneGroup.getGroupName());
                    newGroup.setCreatedBy(userProfile.getUserId());
                    newGroup.setGroupARN(arn);
                    newGroup.setStatus(Constants.GROUP_STATUS.CREATED.name());
                    newGroup.setTimestamp(System.currentTimeMillis());
                    newGroup.setGroupServerId(oneGroup.getCourseCode());
                    fbDb.saveGroup(newGroup);
                    ltDb.saveGroup(newGroup);

                    String subscriptionId = sns.subscribeToTopic(userProfile.getArn(), arn);

                    GroupMember groupMember = new GroupMember();
                    groupMember.setSubscriptionId(subscriptionId);
                    groupMember.setTimestamp(System.currentTimeMillis());
                    groupMember.setGroupId(newGroup.getGroupServerId());
                    groupMember.setUserARN(userProfile.getArn());
                    groupMember.setUserId(userProfile.getUserId());
                    fbDb.saveGroupMember(groupMember);
                    ltDb.saveGroupMember(groupMember);

                    //send others message of group member update
                    Map<String, String> message = new HashMap<String, String>();
                    message.put(Constants.REFRESH_MEMBERS, "" + true);
                    message.put(LearnTogetherContract.GroupMemberEntry.GROUP_ID, newGroup.getGroupServerId());
                    Gson gson = new Gson();
                    sns.publish(arn, gson.toJson(message));

                    return true;
                } else
                    return false;
            }

            @Override
            protected void onPostExecute(Object o) {
                progress.dismiss();
                boolean result = (boolean) o;
                if (result) {
                    Toast.makeText(getActivity().getApplicationContext(), "Course Added", Toast.LENGTH_SHORT).show();
                    if (mCourseListView != null) {
                        courseAdapter = new CourseAdapter(getContext(), ltDb.getCourseGroups());
                        mCourseListView.setAdapter(courseAdapter);
                    }
                } else
                    Toast.makeText(getActivity().getApplicationContext(), "Unable to add course, Contact administrator", Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    private void registerExistingGroupAWS(final Group oneGroup, final Context context) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                if (userProfile != null) {
                    AWSSNSWrapper sns = new AWSSNSWrapper(context);

                    String subscriptionId = sns.subscribeToTopic(userProfile.getArn(), oneGroup.getGroupARN());

                    GroupMember groupMember = new GroupMember();
                    groupMember.setSubscriptionId(subscriptionId);
                    groupMember.setTimestamp(System.currentTimeMillis());
                    groupMember.setGroupId(oneGroup.getGroupServerId());
                    groupMember.setUserARN(userProfile.getArn());
                    groupMember.setUserId(userProfile.getUserId());

                    //send others message of group member update
                    Map<String, String> message = new HashMap<String, String>();
                    message.put(Constants.REFRESH_MEMBERS, "" + true);
                    message.put(LearnTogetherContract.GroupMemberEntry.GROUP_ID, oneGroup.getGroupServerId());
                    Gson gson = new Gson();
                    sns.publish(oneGroup.getGroupARN(), gson.toJson(message));

                    fbDb.saveGroupMember(groupMember);
                    ltDb.saveGroupMember(groupMember);

                    return true;
                } else
                    return false;
            }

            @Override
            protected void onPostExecute(Object o) {
                progress.dismiss();
                boolean result = (boolean) o;
                if (result) {
                    Toast.makeText(getActivity().getApplicationContext(), "Course Added", Toast.LENGTH_SHORT).show();
                    if (mCourseListView != null) {
                        courseAdapter = new CourseAdapter(getContext(), ltDb.getCourseGroups());
                        mCourseListView.setAdapter(courseAdapter);
                    }
                } else
                    Toast.makeText(getActivity().getApplicationContext(), "Unable to add course, Contact administrator", Toast.LENGTH_SHORT).show();
            }
        }.execute();


    }

    public static void refreshList() {
        if (mCourseListView != null && context != null) {
            CourseAdapter courseAdapter = new CourseAdapter(context, ltDb.getCourseGroups());
            mCourseListView.setAdapter(courseAdapter);
        }
    }
}
