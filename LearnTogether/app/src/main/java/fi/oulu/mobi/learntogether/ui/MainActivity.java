package fi.oulu.mobi.learntogether.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

import java.io.File;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Technology;

import static fi.oulu.mobi.learntogether.helpers.Constants.FOLDER_DELIMITER;
import static fi.oulu.mobi.learntogether.helpers.Constants.PI_NAME;
import static fi.oulu.mobi.learntogether.helpers.Constants.PROFILE_FOLDER_NAME;

/**
 * Created by raniq on 4/1/2017.
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    SharedPreferences preferences;
    fi.oulu.mobi.learntogether.helpers.UserProfile userProfile = null;
    NavigationView navigationView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Stetho.initializeWithDefaults(this);
        LearnTogetherContract ltDb = new LearnTogetherContract(this);

        preferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        if (!preferences.contains("register")) {
            boolean checkValue = preferences.getBoolean("register", false);
            if (!checkValue) {
                Intent newIntent = new Intent(this, UserProfile.class);
                startActivity(newIntent);
                finish();
            } else {
                userProfile = ltDb.getUserProfile(preferences.getString("userId", ""));
                initialize(toolbar);
            }
        } else {
            userProfile = ltDb.getUserProfile(preferences.getString("userId", ""));
            initialize(toolbar);
        }
    }

    /**
     * Returns the currently displayed fragment.
     *
     * @return Fragment or null.
     */
    private Fragment getFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.flContent);
        return fragment;
    }

    private void setFragmentTitle(Fragment fragment) {
        // Set the activity title

    }

    private void initialize(Toolbar toolbar) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        updateView();

        FragmentManager fragmentManager = getSupportFragmentManager();
        try {
            fragmentManager.beginTransaction().replace(R.id.flContent,
                    ((Fragment) HomeFragment.class.newInstance())).commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void updateView() {
        if (navigationView != null) {
            View hView = navigationView.getHeaderView(0);
            ImageView profileImage = (ImageView) hView.findViewById(R.id.profilePicture);
            TextView name = (TextView) hView.findViewById(R.id.name);
            TextView studentId = (TextView) hView.findViewById(R.id.studentid);
            profileImage.setOnClickListener(this);

            String path = Environment.getExternalStorageDirectory().toString();
            path = path + FOLDER_DELIMITER + Constants.FOLDER_NAME + FOLDER_DELIMITER + PROFILE_FOLDER_NAME +
                    FOLDER_DELIMITER + PI_NAME;
            if (Technology.getBitmap(path) != null)
                profileImage.setImageBitmap(Technology.getBitmap(path));

            if (userProfile != null) {
                name.setText(userProfile.getFirstName() + " " + userProfile.getLastName());
                studentId.setText("" + userProfile.getStudentId());
            }
        }
    }

    @Override
    protected void onResume() {
        updateView();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;

        if (id == R.id.nav_my_courses) {
            fragmentClass = MyCourses.class;
            setFragment(fragment, fragmentClass);
            StateMachine.setCurrentState(StateMachine.States.COURSES);
        } else if (id == R.id.nav_home) {
            fragmentClass = HomeFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            StateMachine.setCurrentState(StateMachine.States.HOME);

        } else if (id == R.id.nav_buddies) {
            fragmentClass = MyBuddies.class;
            setFragment(fragment, fragmentClass);
            StateMachine.setCurrentState(StateMachine.States.BUDDIES);

        } else if (id == R.id.nav_groups) {
            fragmentClass = MyGroupFragment.class;
            setFragment(fragment, fragmentClass);
            StateMachine.setCurrentState(StateMachine.States.GROUPS);

        } else if (id == R.id.nav_settings) {
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new SettingsFragment()).addToBackStack(Constants.HOME_FRAGMENT).commit();
            StateMachine.setCurrentState(StateMachine.States.SETTINGS);

        } else if (id == R.id.nav_about_us) {
            fragmentClass = AboutUsFragment.class;
            setFragment(fragment, fragmentClass);
            StateMachine.setCurrentState(StateMachine.States.ABOUTUS);
        } else {
            fragmentClass = HomeFragment.class;
            setFragment(fragment, fragmentClass);
        }

        // Highlight the selected item has been done by NavigationView
        item.setChecked(true);
        // Set action bar title
        setTitle(item.getTitle());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setFragment(Fragment fragment, Class fragmentClass) {
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(Constants.HOME_FRAGMENT).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profilePicture:
                Intent newIntent = new Intent(this, UserProfile.class);
                startActivity(newIntent);
                break;
        }
    }
}
