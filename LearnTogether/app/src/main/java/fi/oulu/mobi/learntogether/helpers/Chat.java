package fi.oulu.mobi.learntogether.helpers;

/**
 * Created by raniq on 4/1/2017.
 */

public class Chat {

    int id;
    String serverMessageId;
    String groupId;
    String userId;
    String message;
    String messageType;
    String messageStatus;
    long timestamp;

    public Chat() {

    }

    public Chat(int id, String serverMessageId, String groupId, String userId, String message, String messageType, String messageStatus, long timestamp) {
        this.id = id;
        this.serverMessageId = serverMessageId;
        this.groupId = groupId;
        this.userId = userId;
        this.message = message;
        this.messageType = messageType;
        this.messageStatus = messageStatus;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerMessageId() {
        return serverMessageId;
    }

    public void setServerMessageId(String serverMessageId) {
        this.serverMessageId = serverMessageId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
