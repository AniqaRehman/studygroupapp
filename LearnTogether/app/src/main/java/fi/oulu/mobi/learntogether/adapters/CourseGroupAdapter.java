package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.ui.ChatFragment;
import fi.oulu.mobi.learntogether.ui.CourseDetailFragment;
import fi.oulu.mobi.learntogether.ui.MainActivity;
import fi.oulu.mobi.learntogether.ui.MeetingsFragment;
import fi.oulu.mobi.learntogether.ui.StateMachine;

/**
 * Created by raniq on 4/2/2017.
 */

public class CourseGroupAdapter extends CursorAdapter {
    LearnTogetherContract learnTogetherContract;

    public CourseGroupAdapter(Context context, Cursor c) {
        super(context, c);
        learnTogetherContract = new LearnTogetherContract(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.course_group_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView nameText = (TextView) view.findViewById(R.id.courseGroupName);
        final String name = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_NAME));
        nameText.setText(name);
        TextView codeText = (TextView) view.findViewById(R.id.courseGroupCode);
        final String code = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.COURSE_CODE));
        codeText.setText(code);

        LinearLayout unreadLayout = (LinearLayout) view.findViewById(R.id.unreadLayout);
        TextView unreadCount = (TextView) view.findViewById(R.id.unreadCount);

        ImageButton detailButton = (ImageButton) view.findViewById(R.id.courseDetails);
        ImageButton chatButton = (ImageButton) view.findViewById(R.id.courseChat);
        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.deleteCourse);
        String groupId = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_SERVER_ID));

        deleteButton.setVisibility(View.GONE);

        chatButton.setOnClickListener(new MyOnClickListener(context, groupId));
        detailButton.setOnClickListener(new MyOnClickListener(context, groupId));

        int count = learnTogetherContract.getUnreadMessageCount(groupId);
        if (count == 0) {
            unreadLayout.setVisibility(View.GONE);
        } else {
            unreadLayout.setVisibility(View.VISIBLE);
            unreadCount.setText("" + count);
        }
    }

    private static class MyOnClickListener implements View.OnClickListener {
        Context mContext;
        String mCode;

        public MyOnClickListener(Context context, String groupId) {
            mContext = context;
            mCode = groupId;
        }

        @Override
        public void onClick(View v) {
            if (mContext instanceof MainActivity) {
                MainActivity mainActivity = (MainActivity) mContext;
                FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();

                switch (v.getId()) {
                    case R.id.courseChat:
                        Fragment chatsFragment = ChatFragment.newInstance(mCode);
                        // Insert the fragment by replacing any existing fragment
                        fragmentManager.beginTransaction().replace(R.id.flContent, chatsFragment).addToBackStack(Constants.MY_GROUP).commit();
                        mainActivity.setTitle(Constants.CHAT_LIST);
                        StateMachine.setCurrentState(StateMachine.States.CHAT_SCREEN);
                        break;
                    case R.id.courseDetails:
                        Fragment courseDetailFragment = CourseDetailFragment.newInstance(mCode, Constants.GROUP_TYPES.COURSE.name());
                        // Insert the fragment by replacing any existing fragment
                        fragmentManager.beginTransaction().replace(R.id.flContent, courseDetailFragment).addToBackStack(Constants.MY_GROUP).commit();
                        mainActivity.setTitle(Constants.COURSE_DETAIL);
                        StateMachine.setCurrentState(StateMachine.States.GROUP_DETAIL);
                        break;
                }
            }
        }
    }
}
