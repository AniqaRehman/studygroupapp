package fi.oulu.mobi.learntogether.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.Constants;

public class BuddyProfileActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CALL = 1;
    LearnTogetherContract ltDb;
    public static final String USER_EXTRA_KEY = "user_id";

    TextView phoneNumber;
    TextView email;
    TextView facebook;
    TextView skype;
    CollapsingToolbarLayout toolbarLayout;

    ImageView profileImage;
    ImageButton phoneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        initializeView();
        ltDb = new LearnTogetherContract(getApplicationContext());
        String userId = getIntent().getExtras().getString(USER_EXTRA_KEY);

        Buddy buddy = new Buddy();
        buddy = ltDb.getBuddy(userId);

        toolbar.setTitle(buddy.getFirstName() + " " + buddy.getLastName());
        setSupportActionBar(toolbar);

        phoneNumber.setText(buddy.getContactNumber());
        email.setText(buddy.getEmail());
        facebook.setText(buddy.getFacebookId());
        skype.setText(buddy.getSkypeId());
        if (!buddy.getProfilePicture().equals("")) {
            File file = new File(buddy.getProfilePicture());
            if (file.exists()) {
                Drawable d = Drawable.createFromPath(buddy.getProfilePicture());
                d = resize(d);
                profileImage.setImageDrawable(d);
            } else {
                // toolbarLayout.setBackgroundResource(R.drawable.default_user_image);
            }
        } else {
            //  toolbarLayout.setBackgroundResource(R.drawable.default_user_image);
        }

        final Buddy finalBuddy = buddy;
        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(BuddyProfileActivity.this,
                        Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(BuddyProfileActivity.this,
                            Manifest.permission.CALL_PHONE)) {
                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(BuddyProfileActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_CALL);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + finalBuddy.getContactNumber()));
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                return;
            }
        }
    }

    private Drawable resize(Drawable image) {
        Bitmap b = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 250, 250, false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }

    private void initializeView() {
        phoneNumber = (TextView) findViewById(R.id.phoneNumber);
        email = (TextView) findViewById(R.id.email);
        facebook = (TextView) findViewById(R.id.facebook);
        skype = (TextView) findViewById(R.id.skype);
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        profileImage = (ImageView) findViewById(R.id.imageProfile);
        phoneButton = (ImageButton) findViewById(R.id.phoneButton);

    }
}
