package fi.oulu.mobi.learntogether.ui;

import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.BuddyRequestListAdapter;
import fi.oulu.mobi.learntogether.adapters.UnreadMessageAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class BuddyRequests extends Fragment {

    public static LearnTogetherContract ltDb;
    static Context context;

    static ListView mBuddyList;

    public BuddyRequests() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ltDb = new LearnTogetherContract(getContext());
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buddy_requests, container, false);
        setHasOptionsMenu(false);
        context = getContext();
        mBuddyList = (ListView) view.findViewById(R.id.home_buddy_list);
        mBuddyList.setAdapter(new BuddyRequestListAdapter(getContext(), ltDb.getBuddyRequests()));
        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        //(getActivity()).setTitle(Constants.BUDDY_REQUESTS);
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.BUDDY_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StateMachine.setCurrentState(StateMachine.States.BUDDY_REQUESTS);

    }

    public static void refreshBuddyRequestList() {
        if (mBuddyList != null) {
            ltDb = new LearnTogetherContract(context);
            mBuddyList.setAdapter(new BuddyRequestListAdapter(context, ltDb.getBuddyRequests()));
        }
    }
}
