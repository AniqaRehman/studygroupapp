package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSS3Wrapper;
import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.ui.MyBuddies;
import fi.oulu.mobi.learntogether.ui.StateMachine;

/**
 * Created by Sam on 4/2/2017.
 */

public class BuddyListAdapter extends CursorAdapter {
    LearnTogetherContract ltDb;

    public BuddyListAdapter(Context context, Cursor c) {
        super(context, c, 0);
        ltDb = new LearnTogetherContract(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_my_buddies, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView firstNameText = (TextView) view.findViewById(R.id.buddy_first_name);
        final String firstname = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.FIRST_NAME));
        firstNameText.setText(firstname);
        TextView lastNameText = (TextView) view.findViewById(R.id.buddy_last_name);
        final String lastname = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.LAST_NAME));
        lastNameText.setText(lastname);

        ImageView pic = (ImageView) view.findViewById(R.id.profilePicture);
        final String profilePicPath = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.PROFILE_PICTURE));
        if(!profilePicPath.equals("")){
            File file = new File(profilePicPath);
            if(file.exists()){
                pic.setImageBitmap(BitmapFactory.decodeFile(profilePicPath));
            }else if(profilePicPath.startsWith(cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.STUDENT_ID)))){
                saveProfilePicture(ltDb.getBuddyFromCursor(cursor), context);
            }
        }
    }


    private String saveProfilePicture(final Buddy buddy, final Context context) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                AWSS3Wrapper s3 = new AWSS3Wrapper(context);
                if (s3.ifFileexists(buddy.getProfilePicture())) {
                    String IMAGE_DIR_PATH = Environment.getExternalStorageDirectory()
                            + File.separator + Constants.FOLDER_NAME + File.separator + Constants.PROFILE_FOLDER_NAME
                            + File.separator + buddy.getUserId() + "/";
                    File imageDirPath = new File(IMAGE_DIR_PATH);

                    if (!imageDirPath.exists()) {
                        imageDirPath.mkdirs();
                    }
                    File file = new File(imageDirPath, Constants.PI_NAME);
                    if (file.exists()) {
                        file.delete();
                    }

                    FileOutputStream out = null;
                    try {
                        InputStream object = s3.getFileFromS3(buddy.getProfilePicture());
                        if (object != null) {
                            out = new FileOutputStream(file);
                            byte[] b = new byte[2048];
                            int length;
                            while ((length = object.read(b)) != -1) {
                                out.write(b, 0, length);
                            }
                            object.close();
                            out.flush();
                            out.close();
                        }
                        return file.getAbsolutePath();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        return "";
                    } catch (IOException e) {
                        e.printStackTrace();
                        return "";
                    }
                }
                return "";
            }

            @Override
            protected void onPostExecute(Object o) {
                String result = (String) o;
                if (!result.isEmpty()) {
                    LearnTogetherContract learnTogetherContract = new LearnTogetherContract(context);
                    learnTogetherContract.updateBuddyProfile(buddy.getUserId(), result);
                    if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                        MyBuddies.runOnUI(new Runnable() {
                            public void run() {
                                try {
                                    MyBuddies
                                            .refreshBuddyList();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                } else {

                }
            }
        }.execute();
        return "";
    }

}
