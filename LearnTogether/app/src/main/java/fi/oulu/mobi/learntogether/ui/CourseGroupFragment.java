package fi.oulu.mobi.learntogether.ui;

import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.CourseAdapter;
import fi.oulu.mobi.learntogether.adapters.CourseGroupAdapter;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * Created by raniq on 4/1/2017.
 */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class CourseGroupFragment extends Fragment {

    static LearnTogetherContract ltDb;
    FirebaseDatabaseManager fbDb;
    static Context context;
    private static ListView mCourseListView;
    private CourseGroupAdapter courseAdapter;

    public CourseGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_course_group, container, false);
        setHasOptionsMenu(false);
        ltDb = new LearnTogetherContract(getActivity().getApplication());
        context = getContext();
        courseAdapter = new CourseGroupAdapter(getContext(), ltDb.getCourseGroups());
        mCourseListView = (ListView) rootView.findViewById(R.id.courseGroups);
        mCourseListView.setAdapter(courseAdapter);
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static void refreshUnreadMessageList() {
        if (mCourseListView != null) {
            ltDb = new LearnTogetherContract(context);
            CourseGroupAdapter courseAdapter = new CourseGroupAdapter(context, ltDb.getCourseGroups());
            mCourseListView.setAdapter(courseAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.CHAT_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StateMachine.setCurrentState(StateMachine.States.COURSE_GROUPS);
    }
}
