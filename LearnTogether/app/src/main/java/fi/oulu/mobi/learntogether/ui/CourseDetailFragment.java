package fi.oulu.mobi.learntogether.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.CourseGroupDetailAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Group;

/**
 * Created by raniq on 4/1/2017.
 */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link CourseDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CourseDetailFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String GROUP_ID = "group_id";
    private static final String GROUP_TYPE = "group_type";
    private static String mGroupid;
    private static String mGroupType;
    private TextView courseName;
    private TextView courseLable;
    static LearnTogetherContract ltDb;

    static ListView groupMember;

    static Context context;

    public CourseDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment CourseDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CourseDetailFragment newInstance(String param1, String groupType) {
        CourseDetailFragment fragment = new CourseDetailFragment();
        Bundle args = new Bundle();
        args.putString(GROUP_ID, param1);
        args.putString(GROUP_TYPE, groupType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGroupid = getArguments().getString(GROUP_ID);
            mGroupType = getArguments().getString(GROUP_TYPE);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_course_detail, container, false);
        setHasOptionsMenu(false);
        courseName = (TextView) rootView.findViewById(R.id.courseName);
        courseLable = (TextView) rootView.findViewById(R.id.group_label);
        if(mGroupType.equals(Constants.GROUP_TYPES.PERSONAL.name())){
            courseLable.setText(R.string.meeting_group_label);
        }else{
            courseLable.setText(R.string.course_group_label);
        }
        ltDb = new LearnTogetherContract(getContext());
        Group group = ltDb.getGroup(mGroupid);
        courseName.setText(group.getGroupName());
        context = getContext();
        groupMember = (ListView) rootView.findViewById(R.id.buddyList);
        groupMember.setAdapter(new CourseGroupDetailAdapter(getContext(), ltDb.getGroupMembers(mGroupid)));

        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static void refreshMembersList() {
        if (groupMember != null && mGroupid != null && !mGroupid.isEmpty() && context != null) {
            ltDb = new LearnTogetherContract(context);
            groupMember.setAdapter(new CourseGroupDetailAdapter(context, ltDb.getGroupMembers(mGroupid)));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.COURSE_DETAIL);
    }
}
