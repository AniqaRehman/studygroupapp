package fi.oulu.mobi.learntogether.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import fi.oulu.mobi.learntogether.services.MessagingService;

public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ConnectivityManager conMan = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMan.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                MessagingService.resendMessages(context);
                Log.d("WifiReceiver", "Have Wifi Connection");
            } else
                Log.d("WifiReceiver", "Don't have Wifi Connection");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
