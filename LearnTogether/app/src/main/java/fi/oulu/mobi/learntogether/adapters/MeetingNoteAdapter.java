package fi.oulu.mobi.learntogether.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSS3Wrapper;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Group;
import fi.oulu.mobi.learntogether.helpers.MeetingNote;
import fi.oulu.mobi.learntogether.ui.MeetingDetails;

/**
 * Created by Sam on 4/23/2017.
 */

public class MeetingNoteAdapter extends BaseAdapter {

    private List<MeetingNote> mNoteItems;
    private Context context;
    private boolean isPrevious;
    public static final String NOTE_CURRENT = "CURRENT";
    private LearnTogetherContract mDatabase;
    public List<Integer> deletedNoteId;
    private ProgressDialog progress;

    public MeetingNoteAdapter(Context context, List<MeetingNote> notes, boolean previous) {
        this.context = context;
        mNoteItems = notes;
        this.isPrevious = previous;
        deletedNoteId = new ArrayList<>();

        mDatabase = new LearnTogetherContract(context);
    }

    @Override
    public int getItemViewType(int position) {
        MeetingNote notes = mNoteItems.get(position);
        if (notes.getId() == -1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getCount() {
        return mNoteItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mNoteItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final MeetingNote note = mNoteItems.get(position);

        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.meeting_note_item, parent, false);
        final ImageView picNote = (ImageView) convertView.findViewById(R.id.picture_note);
        final TextView textNote = (TextView) convertView.findViewById(R.id.text_note);
        final Button removeButton = (Button) convertView.findViewById(R.id.delete_note);
        final Button editButton = (Button) convertView.findViewById(R.id.edit_note);
        final TextView unsaved = (TextView) convertView.findViewById(R.id.unsaved);

        final Button shareButton = (Button) convertView.findViewById(R.id.share_note);
        //hideSharebutton if not saved
        if(note.getId()<=0){
            shareButton.setVisibility(View.INVISIBLE);
            unsaved.setVisibility(View.VISIBLE);
        }else{
            shareButton.setVisibility(View.VISIBLE);
            unsaved.setVisibility(View.INVISIBLE);
        }

        if (isPrevious) {
            removeButton.setVisibility(View.INVISIBLE);
            editButton.setVisibility(View.INVISIBLE);
            shareButton.setVisibility(View.INVISIBLE);
        } else {
            shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (note.getId() > 0) {
                        progress = progress.show(context, "Meeting note", "Sharing note...");
                        Group group = mDatabase.getGroup(mDatabase.getMeeting(note.getServerMeetingId()).getGroupId());
                        sendNote(note.toMap(), context, group.getGroupARN(), v);
                        shareButton.setEnabled(false);
                    } else {
                        Toast.makeText(
                                context,
                                ((MeetingDetails) context).getResources().getString(R.string.first_save_note),
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mNoteItems.remove(position);
                    notifyDataSetChanged();
                    if (note.getId() > 0) {
                        mDatabase.deleteMeetingNote(note.getId());
                    }
                }
            });
            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final View dialoglayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_add_note, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setView(dialoglayout);
                    builder.setTitle(Constants.ADD_NOTE);
                    final EditText noteText = (EditText) dialoglayout.findViewById(R.id.text_note);
                    noteText.setText(note.getNote());
                    noteText.setSelection(noteText.getText().length());
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            note.setNote(noteText.getText().toString());
                            unsaved.setVisibility(View.VISIBLE);
                        }

                    });
                    final AlertDialog mAlertDialog = builder.create();
                    mAlertDialog.show();
                }
            });
        }
        if(note.getNoteType().equals(Constants.NOTE_TYPE.PICTURE.name())) {
            editButton.setVisibility(View.GONE);
            picNote.setVisibility(View.VISIBLE);
            if (note.getNote().equals(NOTE_CURRENT)) {
                picNote.setImageBitmap(note.getImageBitMap());
            } else {
                picNote.setImageBitmap(BitmapFactory.decodeFile(note.getNote()));
            }
        }else if(note.getNoteType().equals(Constants.NOTE_TYPE.TEXT.name())) {
            editButton.setVisibility(View.VISIBLE);
            textNote.setText(note.getNote());
            textNote.setVisibility(View.VISIBLE);
            picNote.setVisibility(View.GONE);
        }

        return convertView;
    }

    private void sendNote(final Map<String, String> messagePacket, final Context context, final String groupArn, final View view) {
        new AsyncTask<Object, Object, Object>() {
            protected Object doInBackground(Object... params) {
                try {
                    //upload the image if it is image note
                    if (messagePacket.get(LearnTogetherContract.MeetingNotes.NOTE_TYPE).equals(Constants.NOTE_TYPE.PICTURE.name())) {
                        AWSS3Wrapper s3 = new AWSS3Wrapper(context);
                        String path = s3.uploadFileOnS3(messagePacket.get(LearnTogetherContract.MeetingNotes.SERVER_MEETING_ID),
                                messagePacket.get(LearnTogetherContract.MeetingNotes.SERVER_NOTE_ID) + ".jpg", messagePacket.get(LearnTogetherContract.MeetingNotes.NOTE));
                        messagePacket.put(LearnTogetherContract.MeetingNotes.REMOTE_LOCATION, path);
                    }
                    AWSSNSWrapper sns = new AWSSNSWrapper(context);
                    Gson gson = new Gson();
                    sns.publish(groupArn, gson.toJson(messagePacket));
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean result = (boolean) o;
                progress.dismiss();
                if (!result) {
                    Toast.makeText(
                            context,
                            ((MeetingDetails) context).getResources().getString(R.string.error_contact_administrator),
                            Toast.LENGTH_LONG).show();
                }
                view.setEnabled(true);
            }
        }.execute();
    }

    public List<Integer> getDeletedNoteId() {
        return deletedNoteId;
    }

    public void setDeletedNoteId(List<Integer> deletedNoteId) {
        this.deletedNoteId = deletedNoteId;
    }
}
