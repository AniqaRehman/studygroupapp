package fi.oulu.mobi.learntogether.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fi.oulu.mobi.learntogether.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PersonalGroupDetail#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalGroupDetail extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String GROUP_NAME = "param1";
    private static final String GROUP_ID = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public PersonalGroupDetail() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PersonalGroupDetail.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonalGroupDetail newInstance(String groupName, int groupId) {
        PersonalGroupDetail fragment = new PersonalGroupDetail();
        Bundle args = new Bundle();
        args.putString(GROUP_NAME, groupName);
        args.putInt(GROUP_ID, groupId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(GROUP_NAME);
            mParam2 = getArguments().getString(GROUP_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_course_detail, container, false);
    }

}
