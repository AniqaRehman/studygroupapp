package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSS3Wrapper;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.ui.BuddyRequests;
import fi.oulu.mobi.learntogether.ui.HomeFragment;
import fi.oulu.mobi.learntogether.ui.MainActivity;
import fi.oulu.mobi.learntogether.ui.MyBuddies;
import fi.oulu.mobi.learntogether.ui.StateMachine;

/**
 * Created by raniq on 4/25/2017.
 */

public class BuddyRequestListAdapter extends CursorAdapter {
    LearnTogetherContract learnTogetherContract;

    public BuddyRequestListAdapter(Context context, Cursor c) {
        super(context, c);
        learnTogetherContract = new LearnTogetherContract(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_buddy_request, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        GroupMember groupMember = new GroupMember();
        groupMember = learnTogetherContract.getGroupMember(cursor.getString(cursor.getColumnIndex(LearnTogetherContract.BuddyRequestEntry.USER_ID)));

        TextView nameText = (TextView) view.findViewById(R.id.buddyName);
        String name = "";
        if (groupMember != null && groupMember.getId() != 0)
            name = groupMember.getUserFirstName() + " " + groupMember.getUserLastName();
        nameText.setText(name);

        Button acceptButton = (Button) view.findViewById(R.id.acceptBuddy);
        Button cancelButton = (Button) view.findViewById(R.id.rejectBuddy);

        acceptButton.setOnClickListener(new BuddyRequestListAdapter.MyOnClickListener(context, groupMember.getUserId(), groupMember.getUserARN()));
        cancelButton.setOnClickListener(new BuddyRequestListAdapter.MyOnClickListener(context, groupMember.getUserId(), groupMember.getUserARN()));

    }

    private static class MyOnClickListener implements View.OnClickListener {
        Context mContext;
        String mUserId;
        String mUserArn;
        static LearnTogetherContract learnTogetherContract;
        SharedPreferences preferences;

        public MyOnClickListener(Context context, String userId, String userArn) {
            mContext = context;
            mUserId = userId;
            mUserArn = userArn;
            learnTogetherContract = new LearnTogetherContract(context);
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        @Override
        public void onClick(View v) {
            if (mContext instanceof MainActivity) {
                switch (v.getId()) {
                    case R.id.acceptBuddy:
                        learnTogetherContract.updateBuddyStatus(mUserId, Constants.BUDDY_REQUEST_STATUS.PENDING.name());
                        sendMessage(createMessagePacket(mUserId, true), mUserArn, mContext);
                        BuddyRequests.refreshBuddyRequestList();
                        break;
                    case R.id.rejectBuddy:
                        learnTogetherContract.updateBuddyStatus(mUserId, Constants.BUDDY_REQUEST_STATUS.REJECTED.name());
                        sendMessage(createMessagePacket(mUserId, false), mUserArn, mContext);
                        BuddyRequests.refreshBuddyRequestList();
                        break;
                }
            }
        }

        private static void getBuddyData(String userId, final Context context) {
            FirebaseDatabaseManager fbDb = new FirebaseDatabaseManager();
            fbDb.getUser(userId).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                Buddy buddy = dataSnapshot.getValue(Buddy.class);
                                buddy.setUserId(dataSnapshot.getKey());
                                saveProfilePicture(buddy, context);
                                learnTogetherContract.addBuddies(buddy);
                                if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                                    MyBuddies.runOnUI(new Runnable() {
                                        public void run() {
                                            try {
                                                MyBuddies
                                                        .refreshBuddyList();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // read query is cancelled.
                        }
                    });

        }

        private static String saveProfilePicture(final Buddy buddy, final Context context) {
            new AsyncTask<Object, Object, Object>() {

                protected Object doInBackground(Object... params) {
                    AWSS3Wrapper s3 = new AWSS3Wrapper(context);
                    if (s3.ifFileexists(buddy.getProfilePicture())) {
                        String IMAGE_DIR_PATH = Environment.getExternalStorageDirectory()
                                + File.separator + Constants.FOLDER_NAME + File.separator + Constants.PROFILE_FOLDER_NAME
                                + File.separator + buddy.getUserId() + "/";
                        File imageDirPath = new File(IMAGE_DIR_PATH);

                        if (!imageDirPath.exists()) {
                            imageDirPath.mkdirs();
                        }
                        File file = new File(imageDirPath, Constants.PI_NAME);
                        if (file.exists()) {
                            file.delete();
                        }

                        FileOutputStream out = null;
                        try {
                            InputStream object = s3.getFileFromS3(buddy.getProfilePicture());
                            if (object != null) {
                                out = new FileOutputStream(file);
                                byte[] b = new byte[2048];
                                int length;
                                while ((length = object.read(b)) != -1) {
                                    out.write(b, 0, length);
                                }
                                object.close();
                                out.flush();
                                out.close();
                            }
                            return file.getAbsolutePath();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            return "";
                        } catch (IOException e) {
                            e.printStackTrace();
                            return "";
                        }
                    }
                    return "";
                }

                @Override
                protected void onPostExecute(Object o) {
                    String result = (String) o;
                    if (!result.isEmpty()) {
                        LearnTogetherContract learnTogetherContract = new LearnTogetherContract(context);
                        learnTogetherContract.updateBuddyProfile(buddy.getUserId(), result);
                        if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                            MyBuddies.runOnUI(new Runnable() {
                                public void run() {
                                    try {
                                        MyBuddies
                                                .refreshBuddyList();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                    } else {

                    }
                }
            }.execute();
            return "";
        }
        private String createMessagePacket(String mUserId, boolean accepted) {
            String message = "";
            Map<String, String> messageMap = new HashMap<>();
            if (accepted)
                messageMap.put(LearnTogetherContract.BuddyRequestEntry.ACCEPTED, "" + true);
            else
                messageMap.put(LearnTogetherContract.BuddyRequestEntry.REJECTED, "" + true);
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.USER_ID, preferences.getString("userId", ""));
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.TIMESTAMP, "" + System.currentTimeMillis());
            messageMap.put(LearnTogetherContract.BuddyRequestEntry.RECEVER_ID, mUserId);

            Gson gson = new Gson();
            message = gson.toJson(messageMap);

            return message;
        }

        private static void sendMessage(final String messagePacket, final String mUserArn, final Context context) {
            new AsyncTask<Object, Object, Object>() {
                protected Object doInBackground(Object... params) {
                    try {

                        AWSSNSWrapper sns = new AWSSNSWrapper(context);
                        sns.publish(mUserArn, messagePacket);
                        Gson gson = new Gson();
                        Map<String, String> map = new HashMap<>();
                        map = (Map<String, String>) gson.fromJson(messagePacket, map.getClass());
                        if (map.containsKey(LearnTogetherContract.BuddyRequestEntry.ACCEPTED))
                            getBuddyData(map.get(LearnTogetherContract.BuddyRequestEntry.RECEVER_ID), context);
                        learnTogetherContract = new LearnTogetherContract(context);
                        learnTogetherContract.updateBuddyStatus(map.get(LearnTogetherContract.BuddyRequestEntry.RECEVER_ID), Constants.BUDDY_REQUEST_STATUS.DELIVERED.name());
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                }

                @Override
                protected void onPostExecute(Object o) {
                    boolean result = (boolean) o;
                }
            }.execute();
        }
    }
}
