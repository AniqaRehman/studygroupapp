package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Chat;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.helpers.Technology;

/**
 * Created by raniq on 4/23/2017.
 */

public class ChatListAdapter extends BaseAdapter {

    private Context context;
    private final List<Chat> mValues;
    private LearnTogetherContract ltDb;

    public ChatListAdapter(Context context, List<Chat> mValues) {
        this.context = context;
        this.mValues = mValues;
        this.ltDb = new LearnTogetherContract(context);
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public int getItemViewType(int position) {
        Chat entry = mValues.get(position);
        if (entry.getId() == -1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Chat getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void refill(List<Chat> events) {
        mValues.clear();
        mValues.addAll(events);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Chat entry = mValues.get(position);
        convertView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_chat, parent, false);
        ViewHolder holder = new ViewHolder(convertView);
        holder.mItem = mValues.get(position);

        if (getItemViewType(position) == 0) {
            holder.listContent.setVisibility(View.VISIBLE);
            holder.seperator.setVisibility(View.GONE);

            if (!mValues.get(position).getMessageStatus().equals(Constants.MESSAGE_STATUS.READ.name()) && !mValues.get(position).getMessageStatus().equals(Constants.MESSAGE_STATUS.UNREAD.name())) {
                holder.listContent.setBackgroundResource(R.drawable.bubble_in);
                holder.listItem.setGravity(Gravity.RIGHT);
                holder.userName.setText("Me:");

            } else {
                holder.listContent.setBackgroundResource(R.drawable.bubble_out);
                holder.listItem.setGravity(Gravity.LEFT);

                GroupMember groupMember = ltDb.getGroupMember(holder.mItem.getUserId());
                holder.userName.setText(groupMember.getUserFirstName() + " " + groupMember.getUserLastName());
                //get name for user

            }
            holder.textContent.setText(holder.mItem.getMessage());
            holder.textTime.setText((Technology.getTimeInFormat(holder.mItem.getTimestamp()).split("\\s")[1]));

        } else {
            holder.listContent.setVisibility(View.GONE);
            holder.seperator.setVisibility(View.VISIBLE);
            holder.date.setText((Technology.getTimeInFormat(holder.mItem.getTimestamp()).split("\\s")[0]));
        }

        convertView.setTag(holder);
        return convertView;
    }

    private class ViewHolder {
        public final View mView;
        public final TextView userName;
        public final TextView textContent;
        public final TextView textTime;
        public final TextView date;
        public final LinearLayout seperator;
        public final LinearLayout listContent;
        public final LinearLayout listItem;
        public Chat mItem;

        public ViewHolder(View view) {
            mView = view;
            userName = (TextView) view.findViewById(R.id.userName);
            textContent = (TextView) view.findViewById(R.id.textContent);
            textTime = (TextView) view.findViewById(R.id.textTime);
            date = (TextView) view.findViewById(R.id.date);
            seperator = (LinearLayout) view.findViewById(R.id.seperator);
            listContent = (LinearLayout) view.findViewById(R.id.listContent);
            listItem = (LinearLayout) view.findViewById(R.id.listItem);
        }
    }

    public void remove(Chat object) {
        mValues.remove(object);
        notifyDataSetChanged();
    }
}
