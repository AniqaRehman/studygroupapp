package fi.oulu.mobi.learntogether.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Group;
import fi.oulu.mobi.learntogether.helpers.Meeting;

public class AddMeeting extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {


    public static final String ADD_MEETING_EXTRA = "ADD_MEETING";
    public static final int LOCATION_REQUEST_CODE = 1001;
    private EditText mMeetingDate;
    private EditText mMeetingTime;
    private EditText mMeetingTitle;
    private EditText mMeetingDesc;
    private EditText mMeetingLocation;
    private String mMeetingLocationSelected = "0_0";
    private Button mLocationBtn;
    private Button mSaveBtn;
    private Calendar mNewMeetingTime = Calendar.getInstance();
    private String groupId = "DefaultGroupID";
    private String userId = "DefaultUserId";
    public LearnTogetherContract ltDB;
    private static ProgressDialog progress;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meeting);
        mContext =this;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        userId = prefs.getString("userId", "Not Registered.");

        groupId = getIntent().getExtras().getString(HomeFragment.MEETING_GROUP_EXTRA);

        mMeetingTitle = (EditText) findViewById(R.id.meeting_title);
        mMeetingDesc = (EditText) findViewById(R.id.meeting_desc);
        mMeetingLocation = (EditText) findViewById(R.id.meeting_location);
        Calendar time = Calendar.getInstance();
        ltDB = new LearnTogetherContract(getApplicationContext());

        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                time.get(Calendar.YEAR), time.get(Calendar.MONTH), time.get(Calendar.DAY_OF_MONTH));
        mMeetingDate = (EditText) findViewById(R.id.meeting_date);
        mMeetingDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    datePickerDialog.show();
                    return true;
                }
                return false;
            }
        });

        final TimePickerDialog timePickerDialog =
                new TimePickerDialog(this, this, time.get(Calendar.HOUR_OF_DAY),
                        time.get(Calendar.MINUTE), true);
        mMeetingTime = (EditText) findViewById(R.id.meeting_time);
        mMeetingTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    timePickerDialog.show();
                    return true;
                }
                return false;
            }
        });

        mLocationBtn = (Button) findViewById(R.id.new_meeting_location);
        mLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapActivityIntent = new Intent(getApplicationContext(), MeetingLocation.class);
                mapActivityIntent.putExtra(MeetingLocation.MAP_FOR, ADD_MEETING_EXTRA);
                startActivityForResult(mapActivityIntent, LOCATION_REQUEST_CODE);
            }
        });

        mSaveBtn = (Button) findViewById(R.id.save_meeting);
        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress = progress.show(mContext, "Add Meeting", "Creating...");
                Meeting meeting = new Meeting();
                meeting.setGroupId(groupId);
                meeting.setUserId(userId);
                meeting.setDescription(mMeetingDesc.getText().toString());
                meeting.setTitle(mMeetingTitle.getText().toString());
                meeting.setTimestamp(mNewMeetingTime.getTimeInMillis());
                meeting.setLocation(mMeetingLocation.getText().toString() + "_" + mMeetingLocationSelected);
                meeting.setServerMeetingId(mNewMeetingTime.getTimeInMillis() + "_" + groupId + "_" + userId);

                Group group = ltDB.getGroup(groupId);
                if (group != null) {
                    Gson gson = new Gson();
                    sendMeeting(gson.toJson(meeting.toMap()), getApplicationContext(), group.getGroupARN(), meeting);
                }
            }
        });

    }

    private void sendMeeting(final String messagePacket, final Context context, final String groupArn, final Meeting meeting) {
        new AsyncTask<Object, Object, Object>() {
            protected Object doInBackground(Object... params) {
                try {
                    AWSSNSWrapper sns = new AWSSNSWrapper(context);
                    sns.publish(groupArn, messagePacket);
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                progress.dismiss();
                boolean result = (boolean) o;
                if (!result) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.error_contact_administrator),
                            Toast.LENGTH_LONG).show();
                }else{
                    saveToDatabase(meeting);
                    finish();
                }
            }
        }.execute();
    }

    private void saveToDatabase(Meeting meeting) {
        //Adding data to local database
        LearnTogetherContract ltDb = new LearnTogetherContract(this);
        try {
            if (ltDb.addMeeting(meeting)) {
                Toast.makeText(
                        getApplicationContext(),
                        getResources().getString(R.string.meeting_saved),
                        Toast.LENGTH_SHORT).show();
            }
        } catch (SQLiteException se) {
            se.printStackTrace();
            Toast.makeText(
                    getApplicationContext(),
                    getResources().getString(R.string.error_contact_administrator),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mMeetingDate.setText("" + year + "-" + (month + 1) + "-" + dayOfMonth);
        mNewMeetingTime.set(year, month, dayOfMonth);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mMeetingTime.setText("" + hourOfDay + ":" + minute);
        mNewMeetingTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mNewMeetingTime.set(Calendar.MINUTE, minute);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            LatLng selectedLocation = (LatLng) data.getExtras().get(MeetingLocation.LOCATION_RESULT_KEY);
            mMeetingLocationSelected = "" + selectedLocation.latitude + "_" + selectedLocation.longitude;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.ADD_MEETING);
    }
}
