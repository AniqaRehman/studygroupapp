package fi.oulu.mobi.learntogether.helpers;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.database.LearnTogetherContract;

/**
 * Created by raniq on 4/1/2017.
 */

public class Meeting {

    int id;
    String serverMeetingId;
    String groupId;
    String userId;
    String location;
    String title;
    String description;
    long timestamp;

    public Meeting() {

    }

    public Meeting(int id, String serverMeetingId, String groupId, String userId, String location, String title, String description, long timestamp) {
        this.id = id;
        this.serverMeetingId = serverMeetingId;
        this.groupId = groupId;
        this.userId = userId;
        this.location = location;
        this.title = title;
        this.description = description;
        this.timestamp = timestamp;
    }

    public Meeting(Map<String, String> data) {
        this.groupId = data.get(LearnTogetherContract.Meeting.GROUP_ID);
        this.serverMeetingId = data.get(LearnTogetherContract.Meeting.SERVER_MEETING_ID);
        this.description = data.get(LearnTogetherContract.Meeting.DESCRIPTION);
        this.userId = data.get(LearnTogetherContract.Meeting.USER_ID);
        this.title = data.get(LearnTogetherContract.Meeting.TITLE);
        this.timestamp = Long.parseLong(data.get(LearnTogetherContract.Meeting.TIMESTAMP));
        this.location = data.get(LearnTogetherContract.Meeting.LOCATION);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerMeetingId() {
        return serverMeetingId;
    }

    public void setServerMeetingId(String serverMeetingId) {
        this.serverMeetingId = serverMeetingId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put(LearnTogetherContract.Meeting.GROUP_ID, this.groupId);
        map.put(LearnTogetherContract.Meeting.SERVER_MEETING_ID, this.serverMeetingId);
        map.put(LearnTogetherContract.Meeting.DESCRIPTION, this.description);
        map.put(LearnTogetherContract.Meeting.USER_ID, this.userId);
        map.put(LearnTogetherContract.Meeting.TITLE, this.title);
        map.put(LearnTogetherContract.Meeting.TIMESTAMP, "" + this.timestamp);
        map.put(LearnTogetherContract.Meeting.LOCATION, this.location);
        return map;

    }
}
