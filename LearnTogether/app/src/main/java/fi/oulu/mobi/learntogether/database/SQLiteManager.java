package fi.oulu.mobi.learntogether.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by raniq on 4/1/2017.
 */

public class SQLiteManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "learntogether.db";
    private static final int DATABASE_VERSION = 4;
    private static final String SQL_CREATE_USER_PROFILE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.UserProfileEntry.TABLE_NAME +
                    "(" +
                    LearnTogetherContract.UserProfileEntry._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.UserProfileEntry.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.TIMESTAMP + " REAL DEFAULT 0," +
                    LearnTogetherContract.UserProfileEntry.FIRST_NAME + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.LAST_NAME + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.FACEBOOK_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.DATE_OF_BIRTH + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.SKYPE_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.STUDENT_ID + " INTEGER DEFAULT 0," +
                    LearnTogetherContract.UserProfileEntry.CONTACT_NUMBER + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.COUNTRY + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.GCM_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.ARN + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.PROFILE_PICTURE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.EMAIL + " TEXT DEFAULT ''," +
                    LearnTogetherContract.UserProfileEntry.LOCATION + " TEXT DEFAULT ''" +
                    ")";

    private static final String SQL_CREATE_BUDDIES_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.Buddies.TABLE_NAME +
                    "(" +
                    LearnTogetherContract.Buddies._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.Buddies.USER_ID + " TEXT DEFAULT ''," + //key
                    LearnTogetherContract.Buddies.TIMESTAMP + " REAL DEFAULT 0," +
                    LearnTogetherContract.Buddies.FIRST_NAME + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.LAST_NAME + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.FACEBOOK_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.DATE_OF_BIRTH + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.SKYPE_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.STUDENT_ID + " INTEGER DEFAULT 0," +
                    LearnTogetherContract.Buddies.CONTACT_NUMBER + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.COUNTRY + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.GCM_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.ARN + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.PROFILE_PICTURE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.EMAIL + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Buddies.LOCATION + " TEXT DEFAULT ''" +
                    ")";

    private static final String SQL_CREATE_GROUP_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.GroupEntry.TABLE_NAME +
                    "(" + LearnTogetherContract.GroupEntry._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.GroupEntry.GROUP_SERVER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupEntry.GROUP_NAME + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupEntry.GROUP_TYPE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupEntry.COURSE_CODE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupEntry.GROUP_ARN + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupEntry.CREATED_BY + " INTEGER DEFAULT 0," +
                    LearnTogetherContract.GroupEntry.STATUS + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupEntry.TIMESTAMP + " REAL DEFAULT 0" +
                    ")";

    private static final String SQL_CREATE_GROUP_MEMBER_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.GroupMemberEntry.TABLE_NAME +
                    "(" + LearnTogetherContract.GroupMemberEntry._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.GroupMemberEntry.GROUP_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupMemberEntry.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupMemberEntry.TIMESTAMP + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupMemberEntry.SUBSCRIPTION_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupMemberEntry.USER_ARN + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupMemberEntry.FIRST_NAME + " TEXT DEFAULT ''," +
                    LearnTogetherContract.GroupMemberEntry.LAST_NAME + " TEXT DEFAULT ''" +
                    ")";


    private static final String SQL_CREATE_CHAT_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.ChatEntry.TABLE_NAME +
                    "(" + LearnTogetherContract.ChatEntry._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.ChatEntry.SERVER_MESSAGE_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.ChatEntry.GROUP_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.ChatEntry.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.ChatEntry.MESSAGE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.ChatEntry.MESSAGE_TYPE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.ChatEntry.MESSAGE_STATUS + " TEXT DEFAULT ''," +
                    LearnTogetherContract.ChatEntry.TIMESTAMP + " REAL DEFAULT 0" +
                    ")";


    private static final String SQL_CREATE_MEETING_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.Meeting.TABLE_NAME +
                    "(" + LearnTogetherContract.Meeting._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.Meeting.SERVER_MEETING_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Meeting.GROUP_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Meeting.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Meeting.LOCATION + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Meeting.TITLE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Meeting.DESCRIPTION + " TEXT DEFAULT ''," +
                    LearnTogetherContract.Meeting.TIMESTAMP + " REAL DEFAULT 0" +
                    ")";

    private static final String SQL_CREATE_MEETING_NOTE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.MeetingNotes.TABLE_NAME +
                    "(" + LearnTogetherContract.MeetingNotes._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.MeetingNotes.SERVER_MEETING_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.SERVER_NOTE_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.STATUS + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.NOTE_TYPE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.NOTE + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.REMOTE_LOCATION + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.DESCRIPTION + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingNotes.TIMESTAMP + " REAL DEFAULT 0" +
                    ")";

    private static final String SQL_CREATE_MEETING_MEMBER_STATUS_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.MeetingMemberStatusEntry.TABLE_NAME +
                    "(" + LearnTogetherContract.MeetingMemberStatusEntry._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.MeetingMemberStatusEntry.SERVER_MEETING_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingMemberStatusEntry.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingMemberStatusEntry.STATUS + " TEXT DEFAULT ''," +
                    LearnTogetherContract.MeetingMemberStatusEntry.TIMESTAMP + " REAL DEFAULT 0" +
                    ")";

    private static final String SQL_CREATE_BUDDY_REQUEST_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LearnTogetherContract.BuddyRequestEntry.TABLE_NAME +
                    "(" + LearnTogetherContract.BuddyRequestEntry._ID + " INTEGER PRIMARY KEY," +
                    LearnTogetherContract.BuddyRequestEntry.USER_ID + " TEXT DEFAULT ''," +
                    LearnTogetherContract.BuddyRequestEntry.STATUS + " TEXT DEFAULT ''," +
                    LearnTogetherContract.BuddyRequestEntry.TIMESTAMP + " REAL DEFAULT 0" +
                    ")";

    private static final String SQL_DROP_USER_PROFILE_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.UserProfileEntry.TABLE_NAME;
    private static final String SQL_DROP_BUDDIES_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.Buddies.TABLE_NAME;
    private static final String SQL_DROP_CHAT_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.ChatEntry.TABLE_NAME;
    private static final String SQL_DROP_GROUP_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.GroupEntry.TABLE_NAME;
    private static final String SQL_DROP_GROUP_MEMBER_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.GroupMemberEntry.TABLE_NAME;
    private static final String SQL_DROP_MEETING_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.Meeting.TABLE_NAME;
    private static final String SQL_DROP_MEETING_NOTES_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.MeetingNotes.TABLE_NAME;
    private static final String SQL_DROP_MEETING_MEMBER_STATUS_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.MeetingMemberStatusEntry.TABLE_NAME;

    private static final String SQL_DROP_BUDDY_REQUEST_TABLE =
            "DROP TABLE IF EXISTS " + LearnTogetherContract.BuddyRequestEntry.TABLE_NAME;

    public SQLiteManager(Context context) {
        super(context, DATABASE_NAME, null,
                DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        System.out.println("Connection to LearnTogether database is open.");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USER_PROFILE_TABLE);
        db.execSQL(SQL_CREATE_BUDDIES_TABLE);
        db.execSQL(SQL_CREATE_CHAT_TABLE);
        db.execSQL(SQL_CREATE_GROUP_MEMBER_TABLE);
        db.execSQL(SQL_CREATE_GROUP_TABLE);
        db.execSQL(SQL_CREATE_MEETING_MEMBER_STATUS_TABLE);
        db.execSQL(SQL_CREATE_MEETING_NOTE_TABLE);
        db.execSQL(SQL_CREATE_MEETING_TABLE);
        db.execSQL(SQL_CREATE_BUDDY_REQUEST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 3) {
            db.execSQL(SQL_DROP_GROUP_MEMBER_TABLE);
            db.execSQL(SQL_CREATE_GROUP_MEMBER_TABLE);
        } else {
            db.execSQL(SQL_DROP_USER_PROFILE_TABLE);
            db.execSQL(SQL_DROP_BUDDIES_TABLE);
            db.execSQL(SQL_DROP_CHAT_TABLE);
            db.execSQL(SQL_DROP_MEETING_TABLE);
            db.execSQL(SQL_DROP_MEETING_NOTES_TABLE);
            db.execSQL(SQL_DROP_MEETING_MEMBER_STATUS_TABLE);
            db.execSQL(SQL_DROP_GROUP_TABLE);
            db.execSQL(SQL_DROP_GROUP_MEMBER_TABLE);
            db.execSQL(SQL_DROP_BUDDY_REQUEST_TABLE);//delete old table
            onCreate(db); //create table again
        }
    }

}
