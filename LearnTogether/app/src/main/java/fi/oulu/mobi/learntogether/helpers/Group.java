package fi.oulu.mobi.learntogether.helpers;

/**
 * Created by raniq on 4/1/2017.
 */

public class Group {

    int id;
    String groupServerId;
    String groupName;
    String groupType;
    String courseCode;
    String groupARN;
    String createdBy;
    String status;
    long timestamp;

    public Group() {
    }

    public Group(int id, String groupServerId, String groupName, String groupType, String courseCode, String groupARN, String createdBy, String status, long timestamp) {
        this.id = id;
        this.groupServerId = groupServerId;
        this.groupName = groupName;
        this.groupType = groupType;
        this.courseCode = courseCode;
        this.groupARN = groupARN;
        this.createdBy = createdBy;
        this.status = status;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupServerId() {
        return groupServerId;
    }

    public void setGroupServerId(String groupServerId) {
        this.groupServerId = groupServerId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getGroupARN() {
        return groupARN;
    }

    public void setGroupARN(String groupARN) {
        this.groupARN = groupARN;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
