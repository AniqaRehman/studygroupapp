package fi.oulu.mobi.learntogether.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.BuddyRequest;
import fi.oulu.mobi.learntogether.helpers.Chat;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Meeting;
import fi.oulu.mobi.learntogether.helpers.Group;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.helpers.MeetingNote;
import fi.oulu.mobi.learntogether.helpers.UserProfile;
import fi.oulu.mobi.learntogether.helpers.Group;

/**
 * Created by raniq on 4/1/2017.
 */

public class LearnTogetherContract {
    static SQLiteDatabase learnTogetherDb = null;
    static SharedPreferences preferences;

    public LearnTogetherContract(Context context) {
        SQLiteManager taskDbManager = new SQLiteManager(context);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (learnTogetherDb == null || !learnTogetherDb.isOpen())
            learnTogetherDb = taskDbManager.getWritableDatabase();
    }

    public boolean updateUserProfile(UserProfile userProfile) {
        learnTogetherDb.delete(UserProfileEntry.TABLE_NAME, null, null);
        return addUserProfile(userProfile);
    }

    public boolean checkCourseGroup(String code) {
        boolean result = false;
        Cursor cursor = learnTogetherDb.query(GroupEntry.TABLE_NAME, null, "" + GroupEntry.COURSE_CODE + " = '" + code + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        }
        return result;
    }

    public Group getGroup(String mGroupId) throws SQLiteException {
        Group group = new Group();
        Cursor cursor = learnTogetherDb.query(GroupEntry.TABLE_NAME, null, "" + GroupEntry.GROUP_SERVER_ID + " = '" + mGroupId + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            group = cursorToGroup(cursor);
        }
        return group;
    }

    private Group cursorToGroup(Cursor cursor) {
        Group group = new Group();
        group.setGroupServerId(cursor.getString(cursor.getColumnIndex(GroupEntry.GROUP_SERVER_ID)));
        group.setGroupType(cursor.getString(cursor.getColumnIndex(GroupEntry.GROUP_TYPE)));
        group.setGroupARN(cursor.getString(cursor.getColumnIndex(GroupEntry.GROUP_ARN)));
        group.setGroupName(cursor.getString(cursor.getColumnIndex(GroupEntry.GROUP_NAME)));
        group.setTimestamp(cursor.getLong(cursor.getColumnIndex(GroupEntry.TIMESTAMP)));
        group.setStatus(cursor.getString(cursor.getColumnIndex(GroupEntry.STATUS)));
        group.setCreatedBy(cursor.getString(cursor.getColumnIndex(GroupEntry.CREATED_BY)));
        group.setCourseCode(cursor.getString(cursor.getColumnIndex(GroupEntry.COURSE_CODE)));
        group.setId(cursor.getInt(cursor.getColumnIndex(GroupEntry._ID)));
        return group;
    }

    public List<Chat> getChatList(String mGroupId) {
        List<Chat> chatList = new ArrayList<Chat>();
        Cursor cursor = learnTogetherDb.query(ChatEntry.TABLE_NAME, null, "" + ChatEntry.GROUP_ID + " = '" + mGroupId + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                chatList.add(cursorToChat(cursor));
                cursor.moveToNext();
            }
        }
        return chatList;
    }

    private Chat cursorToChat(Cursor cursor) {
        Chat chat = new Chat();
        chat.setId(cursor.getInt(cursor.getColumnIndex(ChatEntry._ID)));
        chat.setGroupId(cursor.getString(cursor.getColumnIndex(ChatEntry.GROUP_ID)));
        chat.setUserId(cursor.getString(cursor.getColumnIndex(ChatEntry.USER_ID)));
        chat.setMessageStatus(cursor.getString(cursor.getColumnIndex(ChatEntry.MESSAGE_STATUS)));
        chat.setMessageType(cursor.getString(cursor.getColumnIndex(ChatEntry.MESSAGE_TYPE)));
        chat.setMessage(cursor.getString(cursor.getColumnIndex(ChatEntry.MESSAGE)));
        chat.setServerMessageId(cursor.getString(cursor.getColumnIndex(ChatEntry.SERVER_MESSAGE_ID)));
        chat.setTimestamp(cursor.getLong(cursor.getColumnIndex(ChatEntry.TIMESTAMP)));
        return chat;
    }

    public void saveChat(Chat newChat) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatEntry.GROUP_ID, newChat.getGroupId());
        contentValues.put(ChatEntry.USER_ID, newChat.getUserId());
        contentValues.put(ChatEntry.MESSAGE_STATUS, newChat.getMessageStatus());
        contentValues.put(ChatEntry.MESSAGE_TYPE, newChat.getMessageType());
        contentValues.put(ChatEntry.MESSAGE, newChat.getMessage());
        contentValues.put(ChatEntry.SERVER_MESSAGE_ID, newChat.getServerMessageId());
        contentValues.put(ChatEntry.TIMESTAMP, newChat.getTimestamp());

        long id = learnTogetherDb.insert(ChatEntry.TABLE_NAME, null, contentValues);

    }

    public void updateMessageStatus(String serverMessageId, String status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatEntry.MESSAGE_STATUS, status);
        learnTogetherDb.update(ChatEntry.TABLE_NAME, contentValues, "" + ChatEntry.SERVER_MESSAGE_ID + " = '" + serverMessageId + "'", null);
    }

    public void updateMessagesStatus(String mGroupId, String status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatEntry.MESSAGE_STATUS, status);
        learnTogetherDb.update(ChatEntry.TABLE_NAME, contentValues, "" + ChatEntry.GROUP_ID + " = '" + mGroupId + "' AND "
                + ChatEntry.MESSAGE_STATUS + " = '" + Constants.MESSAGE_STATUS.UNREAD.name() + "'", null);
    }

    public int getUnreadMessageCount(String groupId) {
        List<Chat> chatList = new ArrayList<Chat>();
        Cursor cursor = learnTogetherDb.query(ChatEntry.TABLE_NAME, null, "" + ChatEntry.GROUP_ID + " = '" + groupId + "' AND "
                + ChatEntry.MESSAGE_STATUS + " = '" + Constants.MESSAGE_STATUS.UNREAD.name() + "'", null, null, null, null);
        return cursor.getCount();

    }

    public Cursor getUnreadMessages() {
        Cursor cursor = learnTogetherDb.query(true, ChatEntry.TABLE_NAME, null, ChatEntry.MESSAGE_STATUS + " = '" + Constants.MESSAGE_STATUS.UNREAD.name() + "'", null,
                ChatEntry.GROUP_ID, null, ChatEntry.TIMESTAMP + " DESC", null);
        return cursor;
    }

    public void updateGroupMember(Buddy buddy) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GroupMemberEntry.USER_ARN, buddy.getArn());
        contentValues.put(GroupMemberEntry.FIRST_NAME, buddy.getFirstName());
        contentValues.put(GroupMemberEntry.LAST_NAME, buddy.getLastName());
        long id = learnTogetherDb.update(GroupMemberEntry.TABLE_NAME, contentValues, GroupMemberEntry.USER_ID + " = '" + buddy.getUserId() + "'", null);
        Log.e("Database", "Update:" + id + " id :" + buddy.getUserId() + " name: " + buddy.getFirstName());
    }

    public void clearGroupMembers(String s) {
        learnTogetherDb.delete(GroupMemberEntry.TABLE_NAME, GroupMemberEntry.GROUP_ID + " = '" + s + "'", null);
    }

    public GroupMember getGroupMember(String userId) {
        GroupMember groupMember = new GroupMember();

        Cursor cursor = learnTogetherDb.query(GroupMemberEntry.TABLE_NAME, null, "" + GroupMemberEntry.USER_ID + " = '" + userId + "'",
                null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            groupMember = cursorToGroupMember(cursor);
        }
        return groupMember;

    }

    private GroupMember cursorToGroupMember(Cursor cursor) {
        GroupMember groupMember = new GroupMember();
        groupMember.setUserId(cursor.getString(cursor.getColumnIndex(GroupMemberEntry.USER_ID)));
        groupMember.setTimestamp(cursor.getLong(cursor.getColumnIndex(GroupMemberEntry.TIMESTAMP)));
        groupMember.setUserARN(cursor.getString(cursor.getColumnIndex(GroupMemberEntry.USER_ARN)));
        groupMember.setGroupId(cursor.getString(cursor.getColumnIndex(GroupMemberEntry.GROUP_ID)));
        groupMember.setSubscriptionId(cursor.getString(cursor.getColumnIndex(GroupMemberEntry.SUBSCRIPTION_ID)));
        groupMember.setUserFirstName(cursor.getString(cursor.getColumnIndex(GroupMemberEntry.FIRST_NAME)));
        groupMember.setUserLastName(cursor.getString(cursor.getColumnIndex(GroupMemberEntry.LAST_NAME)));
        groupMember.setId(cursor.getInt(cursor.getColumnIndex(GroupMemberEntry._ID)));

        return groupMember;
    }

    public Cursor getGroupMembers(String mGroupid) {
        return learnTogetherDb.query(GroupMemberEntry.TABLE_NAME, null, GroupMemberEntry.GROUP_ID + " = '" + mGroupid + "' AND " +
                        GroupMemberEntry.USER_ID + " != '" + preferences.getString("userId", "") + "'",
                null, null, null, GroupMemberEntry.TIMESTAMP + " ASC");
    }

    public void addBuddyRequest(BuddyRequest buddyRequest) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BuddyRequestEntry.USER_ID, buddyRequest.getUserId());
        contentValues.put(BuddyRequestEntry.TIMESTAMP, buddyRequest.getTimestamp());
        contentValues.put(BuddyRequestEntry.STATUS, buddyRequest.getStatus());

        long id = learnTogetherDb.insert(BuddyRequestEntry.TABLE_NAME, null, contentValues);
    }

    public void updateBuddyStatus(String userId, String status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BuddyRequestEntry.STATUS, status);

        long id = learnTogetherDb.update(BuddyRequestEntry.TABLE_NAME, contentValues, BuddyRequestEntry.USER_ID + " = '" + userId + "'", null);
    }

    public Cursor getBuddies() {
        return learnTogetherDb
                .query(LearnTogetherContract.Buddies.TABLE_NAME, null, null
                        , null, null, null, LearnTogetherContract.Buddies.TIMESTAMP + " ASC");
    }

    public Cursor getBuddyRequests() {
        return learnTogetherDb
                .query(LearnTogetherContract.BuddyRequestEntry.TABLE_NAME, null, BuddyRequestEntry.STATUS + " = '" +
                                Constants.BUDDY_REQUEST_STATUS.RECEIVED + "'"
                        , null, null, null, LearnTogetherContract.Buddies.TIMESTAMP + " ASC");

    }

    public boolean checkBuddyRequest(String userId) {
        boolean result = false;
        Cursor cursor = learnTogetherDb.query(BuddyRequestEntry.TABLE_NAME, null, "" + BuddyRequestEntry.USER_ID + " = '" + userId + "'",
                null, null, null, null);
        if (cursor.getCount() > 0) {
            result = true;
        }
        return result;
    }

    public Cursor getPersonalGroups() {
        return learnTogetherDb
                .query(LearnTogetherContract.GroupEntry.TABLE_NAME, null, ""
                                + LearnTogetherContract.GroupEntry.GROUP_TYPE + " = '" + "PERSONAL" + "' "
                        , null, null, null, LearnTogetherContract.Buddies.TIMESTAMP + " ASC");

    }

    public boolean deleteGroup(String mGroupId) {
        return learnTogetherDb.delete(GroupEntry.TABLE_NAME, GroupEntry.GROUP_SERVER_ID + " = '" + mGroupId + "'", null) > 0;
    }

    public String getSubscriptionId(String userId, String mGroupId) {
        GroupMember groupMember = new GroupMember();

        Cursor cursor = learnTogetherDb.query(GroupMemberEntry.TABLE_NAME, null, "" + GroupMemberEntry.USER_ID + " = '" + userId + "' AND " +
                        GroupMemberEntry.GROUP_ID + " = '" + mGroupId + "'",
                null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            groupMember = cursorToGroupMember(cursor);
        }
        return groupMember.getSubscriptionId();
    }

    public Cursor getUpcomingMeetings(String mGroup) {
        return learnTogetherDb.query(LearnTogetherContract.Meeting.TABLE_NAME, null, ""
                        + LearnTogetherContract.Meeting.GROUP_ID + " = '" + mGroup + "' AND "
                        + LearnTogetherContract.Meeting.TIMESTAMP + ">=" + System.currentTimeMillis()
                , null, null, null
                , LearnTogetherContract.Meeting.TIMESTAMP + " ASC");
    }

    public Cursor getPreviousMeetings(String mGroup) {
        return learnTogetherDb.query(LearnTogetherContract.Meeting.TABLE_NAME, null, ""
                        + LearnTogetherContract.Meeting.GROUP_ID + " = '" + mGroup + "' AND "
                        + LearnTogetherContract.Meeting.TIMESTAMP + "<" + System.currentTimeMillis()
                , null, null, null
                , LearnTogetherContract.Meeting.TIMESTAMP + " ASC");
    }

    public fi.oulu.mobi.learntogether.helpers.Meeting getMeeting(String serverMeetingId) {
        fi.oulu.mobi.learntogether.helpers.Meeting meeting = null;
        Cursor cursor = learnTogetherDb.query(Meeting.TABLE_NAME, null,
                Meeting.SERVER_MEETING_ID + "= '" + serverMeetingId + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            meeting = getMeetingFromCursor(cursor);
        }
        return meeting;
    }

    public void updateBuddyProfile(String userId, String result) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Buddies.PROFILE_PICTURE, result);

        long id = learnTogetherDb.update(Buddies.TABLE_NAME, contentValues, Buddies.USER_ID + " = '" + userId + "'", null);
    }

    public List<Chat> getPendingMessages() {
        List<Chat> chats = new ArrayList<>();
        Cursor cursor = learnTogetherDb.query(ChatEntry.TABLE_NAME, null,
                ChatEntry.MESSAGE_STATUS + "= '" + Constants.MESSAGE_STATUS.PENDING.name() + "' ",
                null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                chats.add(cursorToChat(cursor));
            }
        }
        return chats;
    }

    public static class UserProfileEntry {
        public static final String TABLE_NAME = "user_profile";
        public static final String _ID = "_id";
        public static final String USER_ID = "user_id";
        public static final String TIMESTAMP = "timestamp";
        public static final String FIRST_NAME = "firstname";
        public static final String LAST_NAME = "lastname";
        public static final String EMAIL = "email";
        public static final String STUDENT_ID = "student_id";
        public static final String FACEBOOK_ID = "facebook_id";
        public static final String DATE_OF_BIRTH = "date_of_birth";
        public static final String COUNTRY = "country";
        public static final String CONTACT_NUMBER = "countact_number";
        public static final String SKYPE_ID = "skype_id";
        public static final String GCM_ID = "gcm_id";
        public static final String ARN = "arn";
        public static final String PROFILE_PICTURE = "profile_picture";
        public static final String LOCATION = "location";
    }

    public static class Buddies {
        public static final String TABLE_NAME = "buddies";
        public static final String _ID = "_id";
        public static final String USER_ID = "user_id";
        public static final String TIMESTAMP = "timestamp";
        public static final String FIRST_NAME = "firstname";
        public static final String LAST_NAME = "lastname";
        public static final String EMAIL = "email";
        public static final String STUDENT_ID = "student_id";
        public static final String FACEBOOK_ID = "facebook_id";
        public static final String DATE_OF_BIRTH = "date_of_birth";
        public static final String COUNTRY = "country";
        public static final String CONTACT_NUMBER = "countact_number";
        public static final String SKYPE_ID = "skype_id";
        public static final String GCM_ID = "gcm_id";
        public static final String ARN = "arn";
        public static final String PROFILE_PICTURE = "profile_picture";
        public static final String LOCATION = "location";
    }

    public static class GroupEntry {
        public static final String TABLE_NAME = "group_table";
        public static final String _ID = "_id";
        public static final String GROUP_SERVER_ID = "group_server_id";
        public static final String GROUP_NAME = "group_name";
        public static final String GROUP_TYPE = "group_type";
        public static final String COURSE_CODE = "course_code";
        public static final String GROUP_ARN = "group_arn";
        public static final String CREATED_BY = "created_by";
        public static final String STATUS = "status";
        public static final String TIMESTAMP = "timestamp";
    }

    public static class GroupMemberEntry {
        public static final String TABLE_NAME = "group_member";
        public static final String _ID = "_id";
        public static final String GROUP_ID = "group_id";
        public static final String USER_ID = "user_id";
        public static final String TIMESTAMP = "timestamp";
        public static final String SUBSCRIPTION_ID = "subscription_id";
        public static final String USER_ARN = "user_arn";
        public static final String FIRST_NAME = "user_firstname";
        public static final String LAST_NAME = "user_lastname";
    }

    public static class ChatEntry {
        public static final String TABLE_NAME = "chat";
        public static final String _ID = "_id";
        public static final String SERVER_MESSAGE_ID = "server_message_id";
        public static final String GROUP_ID = "group_id";
        public static final String USER_ID = "user_id";
        public static final String MESSAGE = "message";
        public static final String MESSAGE_TYPE = "message_type";
        public static final String MESSAGE_STATUS = "message_status";
        public static final String TIMESTAMP = "timestamp";
    }

    public static class Meeting {
        public static final String TABLE_NAME = "meeting";
        public static final String _ID = "_id";
        public static final String SERVER_MEETING_ID = "server_meeting_id";
        public static final String GROUP_ID = "group_id";
        public static final String USER_ID = "user_id";
        public static final String LOCATION = "location";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String TIMESTAMP = "timestamp";
    }

    public static class MeetingNotes {
        public static final String TABLE_NAME = "meeting_note";
        public static final String _ID = "_id";
        public static final String SERVER_MEETING_ID = "server_meeting_id";
        public static final String SERVER_NOTE_ID = "server_note_id";
        public static final String STATUS = "status";
        public static final String USER_ID = "user_id";
        public static final String NOTE_TYPE = "note_type";
        public static final String NOTE = "note";
        public static final String DESCRIPTION = "description";
        public static final String TIMESTAMP = "timestamp";
        public static final String REMOTE_LOCATION = "remote_location";
    }

    public static class MeetingMemberStatusEntry {
        public static final String TABLE_NAME = "meeting_member_status";
        public static final String _ID = "_id";
        public static final String SERVER_MEETING_ID = "server_meeting_id";
        public static final String USER_ID = "user_id";
        public static final String STATUS = "status";
        public static final String TIMESTAMP = "timestamp";
    }

    public static class BuddyRequestEntry {
        public static final String TABLE_NAME = "buddy_request";
        public static final String _ID = "_id";
        public static final String USER_ID = "user_id";
        public static final String STATUS = "status";
        public static final String TIMESTAMP = "timestamp";
        public static final String RECEVER_ID = "receiver_id";
        public static final String ACCEPTED = "accepted";
        public static final String REJECTED = "rejected";
    }

    // database and table functions
    public void saveGroup(Group newGroup) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GroupEntry.COURSE_CODE, newGroup.getCourseCode());
        contentValues.put(GroupEntry.CREATED_BY, newGroup.getCreatedBy());
        contentValues.put(GroupEntry.GROUP_ARN, newGroup.getGroupARN());
        contentValues.put(GroupEntry.GROUP_NAME, newGroup.getGroupName());
        contentValues.put(GroupEntry.GROUP_SERVER_ID, newGroup.getGroupServerId());
        contentValues.put(GroupEntry.GROUP_TYPE, newGroup.getGroupType());
        contentValues.put(GroupEntry.TIMESTAMP, newGroup.getTimestamp());
        contentValues.put(GroupEntry.STATUS, newGroup.getStatus());

        long id = learnTogetherDb.insert(GroupEntry.TABLE_NAME, null, contentValues);

    }

    public void saveGroupMember(GroupMember groupMember) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GroupMemberEntry.GROUP_ID, groupMember.getGroupId());
        contentValues.put(GroupMemberEntry.SUBSCRIPTION_ID, groupMember.getSubscriptionId());
        contentValues.put(GroupMemberEntry.USER_ARN, groupMember.getUserARN());
        contentValues.put(GroupMemberEntry.USER_ID, groupMember.getUserId());
        contentValues.put(GroupEntry.TIMESTAMP, groupMember.getTimestamp());
        contentValues.put(GroupMemberEntry.FIRST_NAME, groupMember.getUserFirstName());
        contentValues.put(GroupMemberEntry.LAST_NAME, groupMember.getUserLastName());
        contentValues.put(GroupMemberEntry.USER_ARN, groupMember.getUserARN());

        long id = learnTogetherDb.insert(GroupMemberEntry.TABLE_NAME, null, contentValues);
    }


    public boolean addUserProfile(UserProfile userProfile) throws SQLiteException {
        boolean result = false;
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserProfileEntry.USER_ID, userProfile.getUserId());
        contentValues.put(UserProfileEntry.ARN, userProfile.getArn());
        contentValues.put(UserProfileEntry.CONTACT_NUMBER, userProfile.getContactNumber());
        contentValues.put(UserProfileEntry.COUNTRY, userProfile.getCountry());
        contentValues.put(UserProfileEntry.EMAIL, userProfile.getEmail());
        contentValues.put(UserProfileEntry.DATE_OF_BIRTH, userProfile.getDateOfBirth());
        contentValues.put(UserProfileEntry.FACEBOOK_ID, userProfile.getFacebookId());
        contentValues.put(UserProfileEntry.FIRST_NAME, userProfile.getFirstName());
        contentValues.put(UserProfileEntry.GCM_ID, userProfile.getGcmId());
        contentValues.put(UserProfileEntry.LAST_NAME, userProfile.getLastName());
        contentValues.put(UserProfileEntry.LOCATION, userProfile.getLocation());
        contentValues.put(UserProfileEntry.PROFILE_PICTURE, userProfile.getProfilePicture());
        contentValues.put(UserProfileEntry.SKYPE_ID, userProfile.getSkypeId());
        contentValues.put(UserProfileEntry.STUDENT_ID, userProfile.getStudentId());
        contentValues.put(UserProfileEntry.TIMESTAMP, userProfile.getTimestamp());

        long id = learnTogetherDb.insert(UserProfileEntry.TABLE_NAME, null, contentValues);
        if (id > 0)
            result = true;

        return result;
    }

    public UserProfile getUserProfile(String userId) throws SQLiteException {
        UserProfile userProfile = null;
        Cursor cursor = learnTogetherDb.query(UserProfileEntry.TABLE_NAME, null, "" + UserProfileEntry.USER_ID + " = '" + userId + "'", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            userProfile = cursorToUserProfile(cursor);
        }
        return userProfile;
    }

    public Cursor getCourseGroups() throws SQLiteException {
        Cursor cursor = learnTogetherDb.query(LearnTogetherContract.GroupEntry.TABLE_NAME, null, ""
                        + GroupEntry.GROUP_TYPE + " = '" + Constants.GROUP_TYPES.COURSE.name() + "' "
                , null, null, null, LearnTogetherContract.GroupEntry.TIMESTAMP + " ASC");
        return
                cursor;

    }

    private UserProfile cursorToUserProfile(Cursor cursor) {
        UserProfile userProfile = new UserProfile();
        userProfile.setId(cursor.getInt(0));
        userProfile.setUserId(cursor.getString(1));
        userProfile.setTimestamp(cursor.getLong(2));
        userProfile.setFirstName(cursor.getString(3));
        userProfile.setLastName(cursor.getString(4));
        userProfile.setFacebookId(cursor.getString(5));
        userProfile.setDateOfBirth(cursor.getString(6));
        userProfile.setSkypeId(cursor.getString(7));
        userProfile.setStudentId(cursor.getInt(8));
        userProfile.setContactNumber(cursor.getString(9));
        userProfile.setCountry(cursor.getString(10));
        userProfile.setGcmId(cursor.getString(11));
        userProfile.setArn(cursor.getString(12));
        userProfile.setProfilePicture(cursor.getString(13));
        userProfile.setEmail(cursor.getString(14));
        userProfile.setLocation(cursor.getString(15));

        return userProfile;
    }

    public boolean addMeeting(fi.oulu.mobi.learntogether.helpers.Meeting meeting) throws SQLiteException {
        boolean result = false;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Meeting.USER_ID, meeting.getUserId());
        contentValues.put(Meeting.GROUP_ID, meeting.getGroupId());
        contentValues.put(Meeting.SERVER_MEETING_ID, meeting.getServerMeetingId());
        contentValues.put(Meeting.LOCATION, meeting.getLocation());
        contentValues.put(Meeting.TIMESTAMP, meeting.getTimestamp());
        contentValues.put(Meeting.TITLE, meeting.getTitle());
        contentValues.put(Meeting.DESCRIPTION, meeting.getDescription());

        long id = learnTogetherDb.insert(Meeting.TABLE_NAME, null, contentValues);
        if (id > 0)
            result = true;

        return result;
    }

    public boolean addBuddies(Buddy buddies) throws SQLiteException {
        boolean result = false;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Buddies.USER_ID, buddies.getUserId());
        contentValues.put(Buddies.ARN, buddies.getArn());
        contentValues.put(Buddies.CONTACT_NUMBER, buddies.getContactNumber());
        contentValues.put(Buddies.COUNTRY, buddies.getCountry());
        contentValues.put(Buddies.EMAIL, buddies.getEmail());
        contentValues.put(Buddies.DATE_OF_BIRTH, buddies.getDateOfBirth());
        contentValues.put(Buddies.FACEBOOK_ID, buddies.getFacebookId());
        contentValues.put(Buddies.FIRST_NAME, buddies.getFirstName());
        contentValues.put(Buddies.GCM_ID, buddies.getGcmId());
        contentValues.put(Buddies.LAST_NAME, buddies.getLastName());
        contentValues.put(Buddies.LOCATION, buddies.getLocation());
        contentValues.put(Buddies.PROFILE_PICTURE, buddies.getProfilePicture());
        contentValues.put(Buddies.SKYPE_ID, buddies.getSkypeId());
        contentValues.put(Buddies.STUDENT_ID, buddies.getStudentId());
        contentValues.put(Buddies.TIMESTAMP, buddies.getTimestamp());

        long id = learnTogetherDb.insert(Buddies.TABLE_NAME, null, contentValues);
        if (id > 0)
            result = true;

        return result;
    }

    public fi.oulu.mobi.learntogether.helpers.Meeting getMeeting(long meetingId) {
        fi.oulu.mobi.learntogether.helpers.Meeting meeting = null;
        Cursor cursor = learnTogetherDb.query(Meeting.TABLE_NAME, null,
                Meeting._ID + "=" + meetingId, null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            meeting = getMeetingFromCursor(cursor);
        }
        return meeting;
    }

    private fi.oulu.mobi.learntogether.helpers.Meeting getMeetingFromCursor(Cursor cursor) {

        fi.oulu.mobi.learntogether.helpers.Meeting meeting = new fi.oulu.mobi.learntogether.helpers.Meeting();
        meeting.setId(cursor.getInt(0));
        meeting.setServerMeetingId(cursor.getString(1));
        meeting.setGroupId(cursor.getString(2));
        meeting.setUserId(cursor.getString(3));
        meeting.setLocation(cursor.getString(4));
        meeting.setTitle(cursor.getString(5));
        meeting.setDescription(cursor.getString(6));
        meeting.setTimestamp(cursor.getLong(7));

        return meeting;
    }

    public boolean addMeetingNote(MeetingNote meetingNote) {

        boolean result = false;
        ContentValues contentValues = new ContentValues();
        contentValues.put(MeetingNotes.USER_ID, meetingNote.getUserId());
        contentValues.put(MeetingNotes.DESCRIPTION, meetingNote.getDescription());
        contentValues.put(MeetingNotes.SERVER_MEETING_ID, meetingNote.getServerMeetingId());
        contentValues.put(MeetingNotes.NOTE_TYPE, meetingNote.getNoteType());
        contentValues.put(MeetingNotes.TIMESTAMP, meetingNote.getTimestamp());
        contentValues.put(MeetingNotes.NOTE, meetingNote.getNote());
        contentValues.put(MeetingNotes.REMOTE_LOCATION, meetingNote.getRemoteLocation());
        contentValues.put(MeetingNotes.SERVER_NOTE_ID, meetingNote.getServerNoteId());
        contentValues.put(MeetingNotes.STATUS, meetingNote.getDeliveryStatus());

        long id = 0;
        if (!checkNoteExistance(meetingNote)) {
            if (meetingNote.getId() > 0) {
                id = learnTogetherDb.update(MeetingNotes.TABLE_NAME, contentValues,
                        MeetingNotes._ID + "=" + meetingNote.getId(), null);
            } else {
                id = learnTogetherDb.insert(MeetingNotes.TABLE_NAME, null, contentValues);
            }
        } else {
            id = learnTogetherDb.update(MeetingNotes.TABLE_NAME, contentValues,
                    MeetingNotes.SERVER_NOTE_ID + "= '" + meetingNote.getServerNoteId() + "' ", null);
        }

        if (id > 0)
            result = true;
        return result;
    }

    private boolean checkNoteExistance(MeetingNote meetingNote) {
        Cursor cursor = learnTogetherDb.query(MeetingNotes.TABLE_NAME, null,
                MeetingNotes.SERVER_NOTE_ID + "= '" + meetingNote.getServerNoteId() + "' ", null, null, null, null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    public List<MeetingNote> getMeetingNotes(String serverMeetingId) {
        List<MeetingNote> meetingNotes = new ArrayList<>();
        Cursor cursor = learnTogetherDb.query(MeetingNotes.TABLE_NAME, null,
                MeetingNotes.SERVER_MEETING_ID + "= '" + serverMeetingId + "' ", null, null, null, MeetingNotes.TIMESTAMP + " DESC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                meetingNotes.add(getMeetingNoteFromCursor(cursor));
            }
        }
        return meetingNotes;
    }


    public boolean addGroup(fi.oulu.mobi.learntogether.helpers.Group group) throws SQLiteException {
        boolean result = false;
        ContentValues contentValues = new ContentValues();
        contentValues.put(GroupEntry.GROUP_SERVER_ID, group.getGroupServerId());
        contentValues.put(GroupEntry.GROUP_NAME, group.getGroupName());
        contentValues.put(GroupEntry.GROUP_TYPE, group.getGroupType());
        contentValues.put(GroupEntry.COURSE_CODE, group.getCourseCode());
        contentValues.put(GroupEntry.GROUP_ARN, group.getGroupARN());
        contentValues.put(GroupEntry.CREATED_BY, group.getCreatedBy());
        contentValues.put(GroupEntry.STATUS, group.getStatus());
        contentValues.put(GroupEntry.TIMESTAMP, group.getTimestamp());
        contentValues.put(GroupEntry.GROUP_TYPE, group.getGroupType());
        long id = learnTogetherDb.insert(GroupEntry.TABLE_NAME, null, contentValues);
        if (id > 0)
            result = true;

        return result;
    }

    public Buddy getBuddy(String buddyUserId) {
        Buddy buddy = null;
        Cursor cursor = learnTogetherDb.query(Buddies.TABLE_NAME, null,
                Buddies.USER_ID + "= '" + buddyUserId + "' ", null, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            buddy = getBuddyFromCursor(cursor);
        }
        return buddy;
    }

    public Buddy getBuddyFromCursor(Cursor cursor) {
        Buddy buddy = new Buddy();
        buddy.setUserId(cursor.getString(cursor.getColumnIndex(Buddies.USER_ID)));
        buddy.setTimestamp(cursor.getLong(cursor.getColumnIndex(Buddies.TIMESTAMP)));
        buddy.setFirstName(cursor.getString(cursor.getColumnIndex(Buddies.FIRST_NAME)));
        buddy.setLastName(cursor.getString(cursor.getColumnIndex(Buddies.LAST_NAME)));
        buddy.setFacebookId(cursor.getString(cursor.getColumnIndex(Buddies.FACEBOOK_ID)));
        buddy.setDateOfBirth(cursor.getString(cursor.getColumnIndex(Buddies.DATE_OF_BIRTH)));
        buddy.setSkypeId(cursor.getString(cursor.getColumnIndex(Buddies.SKYPE_ID)));
        buddy.setStudentId(cursor.getInt(cursor.getColumnIndex(Buddies.STUDENT_ID)));
        buddy.setContactNumber(cursor.getString(cursor.getColumnIndex(Buddies.CONTACT_NUMBER)));
        buddy.setCountry(cursor.getString(cursor.getColumnIndex(Buddies.COUNTRY)));
        buddy.setGcmId(cursor.getString(cursor.getColumnIndex(Buddies.GCM_ID)));
        buddy.setArn(cursor.getString(cursor.getColumnIndex(Buddies.ARN)));
        buddy.setProfilePicture(cursor.getString(cursor.getColumnIndex(Buddies.PROFILE_PICTURE)));
        buddy.setEmail(cursor.getString(cursor.getColumnIndex(Buddies.EMAIL)));
        buddy.setLocation(cursor.getString(cursor.getColumnIndex(Buddies.LOCATION)));
        return buddy;
    }

    private MeetingNote getMeetingNoteFromCursor(Cursor cursor) {
        MeetingNote note = new MeetingNote();
        note.setNote(cursor.getString(cursor.getColumnIndex(MeetingNotes.NOTE)));
        note.setServerNoteId(cursor.getString(cursor.getColumnIndex(MeetingNotes.SERVER_NOTE_ID)));
        note.setDescription(cursor.getString(cursor.getColumnIndex(MeetingNotes.DESCRIPTION)));
        note.setRemoteLocation(cursor.getString(cursor.getColumnIndex(MeetingNotes.REMOTE_LOCATION)));
        note.setDeliveryStatus(cursor.getString(cursor.getColumnIndex(MeetingNotes.STATUS)));
        note.setTimestamp(cursor.getLong(cursor.getColumnIndex(MeetingNotes.TIMESTAMP)));
        note.setId(cursor.getInt(cursor.getColumnIndex(MeetingNotes._ID)));
        note.setNoteType(cursor.getString(cursor.getColumnIndex(MeetingNotes.NOTE_TYPE)));
        note.setServerMeetingId(cursor.getString(cursor.getColumnIndex(MeetingNotes.SERVER_MEETING_ID)));
        note.setUserId(cursor.getString(cursor.getColumnIndex(MeetingNotes.USER_ID)));
        return note;
    }

    public boolean deleteMeetingNote(int noteId) {
        return learnTogetherDb.delete(MeetingNotes.TABLE_NAME, MeetingNotes._ID + "=" + noteId, null) > 0;
    }
}
