package fi.oulu.mobi.learntogether.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.AddGroupMemberAdapter;
import fi.oulu.mobi.learntogether.adapters.CourseAdapter;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.*;
import fi.oulu.mobi.learntogether.helpers.UserProfile;

public class AddGroupActivity extends AppCompatActivity {
    private ListView mMemberList;
    private AddGroupMemberAdapter memberAdapter;
    LearnTogetherContract ltDb;
    FirebaseDatabaseManager fbDb;
    private String userId;
    private EditText groupName;
    private long currentTimeMillis;
    private Context context;
    Button saveButton;
    private static ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        mMemberList = (ListView) findViewById(R.id.group_member_list);
        ltDb = new LearnTogetherContract(this);
        fbDb = new FirebaseDatabaseManager();
        context = this;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        userId = prefs.getString("userId", "Not Registered.");

        Calendar now = Calendar.getInstance();
        currentTimeMillis = now.getTimeInMillis();

        memberAdapter = new AddGroupMemberAdapter(this, ltDb.getBuddies(), true);
        mMemberList.setAdapter(memberAdapter);

        groupName = (EditText) findViewById(R.id.group_name);

        saveButton = (Button) findViewById(R.id.save_group);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = connMan.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnected()) {
                    progress = progress.show(context, "Add Group", "Registering Group...");
                    saveButton.setEnabled(false);
                    Group savingGroup = new Group();
                    savingGroup.setGroupType(Constants.GROUP_TYPES.PERSONAL.name());
                    savingGroup.setCreatedBy(userId);
                    savingGroup.setGroupName(groupName.getText().toString());
                    savingGroup.setGroupServerId(userId + "_" + groupName.getText().toString() + "_" + currentTimeMillis);
                    savingGroup.setStatus(Constants.GROUP_STATUS.CREATED.name());
                    savingGroup.setTimestamp(currentTimeMillis);
                    savingGroup.setCourseCode(savingGroup.getGroupServerId());

                    List<Buddy> buddyList = getBuddiesFromIds(memberAdapter.getSelectedBuddies());
                    Buddy user = userProfileToBuddyCast(ltDb.getUserProfile(userId));
                    buddyList.add(user);

                    registerGroup(savingGroup, buddyList);
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.no_stable_network),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private Buddy userProfileToBuddyCast(UserProfile userProfile) {
        return new Buddy(userProfile.getId(), userProfile.getUserId(), userProfile.getStudentId(),
                userProfile.getFirstName(), userProfile.getLastName(), userProfile.getEmail(),
                userProfile.getFacebookId(), userProfile.getSkypeId(), userProfile.getDateOfBirth(),
                userProfile.getCountry(), userProfile.getContactNumber(), userProfile.getGcmId(),
                userProfile.getArn(), userProfile.getProfilePicture(), userProfile.getLocation());

    }

    private void registerGroup(final Group oneGroup, final List<Buddy> members) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                if (userId != null) {
                    AWSSNSWrapper sns = new AWSSNSWrapper(getApplicationContext());
                    oneGroup.setGroupARN(sns.createTopic(oneGroup.getGroupServerId()));

                    fbDb.saveGroup(oneGroup);
                    ltDb.saveGroup(oneGroup);

                    for (Buddy buddy : members) {

                        String subscriptionId = sns.subscribeToTopic(buddy.getArn(), oneGroup.getGroupARN());

                        GroupMember groupMember = new GroupMember();
                        groupMember.setSubscriptionId(subscriptionId);
                        groupMember.setTimestamp(currentTimeMillis);
                        groupMember.setGroupId(oneGroup.getGroupServerId());
                        groupMember.setUserARN(buddy.getArn());
                        groupMember.setUserId(buddy.getUserId());
                        groupMember.setUserFirstName(buddy.getFirstName());
                        groupMember.setUserLastName(buddy.getLastName());

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        userId = prefs.getString("userId", "");
                        if (!buddy.getUserId().equals(userId)) {
                            Gson gson = new Gson();
                            sns.publish(buddy.getArn(), gson.toJson(groupMember.toMap()));
                        }

                        fbDb.saveGroupMember(groupMember);
                        ltDb.saveGroupMember(groupMember);
                    }
                    return true;
                } else
                    return false;
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean result = (boolean) o;
                progress.dismiss();
                saveButton.setEnabled(true);
                if (result) {
                    Toast.makeText(context, "Group Added", Toast.LENGTH_SHORT).show();
                    finish();
                } else
                    Toast.makeText(context, "Unable to add Group, Contact administrator", Toast.LENGTH_SHORT).show();
            }
        }.execute();
    }

    private List<Buddy> getBuddiesFromIds(List<String> buddyIds) {
        List<Buddy> buddies = new ArrayList<>();
        for (String buddyId : buddyIds) {
            buddies.add(ltDb.getBuddy(buddyId));
        }
        return buddies;
    }

    @Override
    protected void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.ADD_GROUP);
    }
}
