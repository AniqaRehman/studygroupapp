package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Group;

/**
 * Created by raniq on 4/23/2017.
 */

public class UnreadMessageAdapter extends CursorAdapter {
    LearnTogetherContract learnTogetherContract;

    public UnreadMessageAdapter(Context context, Cursor c) {
        super(context, c);
        learnTogetherContract = new LearnTogetherContract(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.course_group_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Group newGroup = learnTogetherContract.getGroup(cursor.getString(cursor.getColumnIndex(LearnTogetherContract.ChatEntry.GROUP_ID)));
        TextView nameText = (TextView) view.findViewById(R.id.courseGroupName);
        nameText.setText(newGroup.getGroupName());
        TextView codeText = (TextView) view.findViewById(R.id.courseGroupCode);
        codeText.setText(cursor.getString(cursor.getColumnIndex(LearnTogetherContract.ChatEntry.MESSAGE)));

        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.deleteCourse);

        ImageButton detailButton = (ImageButton) view.findViewById(R.id.courseDetails);
        ImageButton chatButton = (ImageButton) view.findViewById(R.id.courseChat);
        detailButton.setVisibility(View.GONE);
        chatButton.setVisibility(View.GONE);
        deleteButton.setVisibility(View.GONE);

        LinearLayout unreadLayout = (LinearLayout) view.findViewById(R.id.unreadLayout);
        TextView unreadCount = (TextView) view.findViewById(R.id.unreadCount);

        int count = learnTogetherContract.getUnreadMessageCount(newGroup.getGroupServerId());
        if (count < 1) {
            unreadLayout.setVisibility(View.GONE);
        } else {
            unreadLayout.setVisibility(View.VISIBLE);
            unreadCount.setText("" + count);
        }
    }
}