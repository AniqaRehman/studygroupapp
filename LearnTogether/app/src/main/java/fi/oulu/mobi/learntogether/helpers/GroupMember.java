package fi.oulu.mobi.learntogether.helpers;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.database.LearnTogetherContract;

/**
 * Created by raniq on 4/1/2017.
 */

public class GroupMember {

    int id;
    String groupId;
    String userId;
    long timestamp;
    String userARN;
    String subscriptionId;
    String userFirstName;
    String userLastName;

    public GroupMember() {
    }

    public GroupMember(int id, String groupId, String userId, long timestamp, String userARN, String subscriptionId) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.timestamp = timestamp;
        this.userARN = userARN;
        this.subscriptionId = subscriptionId;
    }

    public GroupMember(int id, String groupId, String userId, long timestamp, String userARN, String subscriptionId, String userFirstName, String userLastName) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.timestamp = timestamp;
        this.userARN = userARN;
        this.subscriptionId = subscriptionId;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getUserARN() {
        return userARN;
    }

    public void setUserARN(String userARN) {
        this.userARN = userARN;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public Map<String, String> toMap() {
        Map<String, String> groupMember = new HashMap<>();
        groupMember.put(LearnTogetherContract.GroupMemberEntry.GROUP_ID, this.getGroupId());
        groupMember.put(LearnTogetherContract.GroupMemberEntry.FIRST_NAME, this.getGroupId());
        groupMember.put(LearnTogetherContract.GroupMemberEntry.LAST_NAME, this.getGroupId());
        groupMember.put(LearnTogetherContract.GroupMemberEntry.SUBSCRIPTION_ID, this.getGroupId());
        groupMember.put(LearnTogetherContract.GroupMemberEntry.TIMESTAMP, this.getGroupId());
        groupMember.put(LearnTogetherContract.GroupMemberEntry.USER_ID, this.getGroupId());
        groupMember.put(LearnTogetherContract.GroupMemberEntry.USER_ARN, this.getGroupId());
        return groupMember;
    }
}
