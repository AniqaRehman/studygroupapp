package fi.oulu.mobi.learntogether.ui;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Calendar;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.MeetingsListAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MeetingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MeetingsFragment extends Fragment {

    private static final java.lang.String GROUP_ID = "group_id";

    private static String mGroup;
    private TextView mGroupName;
    private static ListView upcomingMeetings;
    private static ListView previousMeeting;
    private String userId;
    private MeetingsListAdapter meetingsListAdapter;
    private MeetingsListAdapter previousMeetingsListAdapter;
    Calendar now;
    public static final String MEETING_LOCATION_EXTRA = "meetingloc";
    public static final String MEETING_LOCATION_FOR_EXTRA = "MeetingsView";

    private static String mGroupId;
    static Context context;
    static LearnTogetherContract ltDb;

    public MeetingsFragment() {
        // Required empty public constructor
    }

    public static MeetingsFragment newInstance(String group, String groupId) {
        MeetingsFragment fragment = new MeetingsFragment();
        Bundle args = new Bundle();
        args.putString("GROUP_NAME", group);
        args.putString(GROUP_ID, groupId);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroup = getArguments().getString("GROUP_NAME");
        mGroupId = getArguments().getString(GROUP_ID);
        now = Calendar.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meetings, container, false);
        setHasOptionsMenu(false);
        context = getContext();
        ltDb = new LearnTogetherContract(context);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        userId = prefs.getString("userId", "Not Registered.");
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addMeetingIntent = new Intent(getContext(), AddMeeting.class);
                addMeetingIntent.putExtra(HomeFragment.MEETING_GROUP_EXTRA, mGroupId);
                startActivity(addMeetingIntent);
            }
        });

        mGroupName = (TextView) view.findViewById(R.id.meeting_group);

        mGroupName.setText(mGroup);
        upcomingMeetings = (ListView) view.findViewById(R.id.upcoming_meetings_list);
        previousMeeting = (ListView) view.findViewById(R.id.previous_meetings_list);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.MEETING_SCREEN);
        refreshList(mGroupId);
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.MEETING_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static void refreshList(String groupId) {
        if (context != null && mGroupId != null && !mGroupId.isEmpty() && mGroupId.equals(groupId)) {

            if (upcomingMeetings != null) {
                MeetingsListAdapter meetingsListAdapter = new MeetingsListAdapter(context, ltDb.getUpcomingMeetings(mGroupId), true, false);
                upcomingMeetings.setAdapter(meetingsListAdapter);
            }

            if (previousMeeting != null) {
                MeetingsListAdapter meetingsListAdapter = new MeetingsListAdapter(context, ltDb.getPreviousMeetings(mGroupId), true, false);
                previousMeeting.setAdapter(meetingsListAdapter);
            }
        }
    }
}
