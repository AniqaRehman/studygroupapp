package fi.oulu.mobi.learntogether.helpers;

/**
 * Created by raniq on 4/1/2017.
 */

public class MeetingMemberStatus {

    int id;
    String serverMeetingId;
    String userId;
    String status;
    long timestamp;

    public MeetingMemberStatus(int id, String serverMeetingId, String userId, String status, long timestamp) {
        this.id = id;
        this.serverMeetingId = serverMeetingId;
        this.userId = userId;
        this.status = status;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerMeetingId() {
        return serverMeetingId;
    }

    public void setServerMeetingId(String serverMeetingId) {
        this.serverMeetingId = serverMeetingId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
