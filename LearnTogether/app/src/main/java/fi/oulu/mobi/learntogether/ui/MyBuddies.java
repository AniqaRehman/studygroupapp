package fi.oulu.mobi.learntogether.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.BuddyListAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link MyBuddies#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyBuddies extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    static LearnTogetherContract ltDb;
    private static ListView mBuddyListView;
    private static CursorAdapter mBuddyListAdapter;
    private String userId;
    static Context context;

    public MyBuddies() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyBuddies.
     */
    public static MyBuddies newInstance(String param1, String param2) {
        MyBuddies fragment = new MyBuddies();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_buddies, container, false);
        setHasOptionsMenu(false);

        Button nfcButton = (Button) view.findViewById(R.id.nfc_button);
        nfcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nfcBeamActivityIntent = new Intent(getContext(), NFCBuddyActivity.class);
                startActivity(nfcBeamActivityIntent);
            }
        });

        context = getContext();
        ltDb = new LearnTogetherContract(getContext());

        mBuddyListView = (ListView) view.findViewById(R.id.buddy_list);
        mBuddyListAdapter = new BuddyListAdapter(getContext(), ltDb.getBuddies());
        mBuddyListView.setAdapter(mBuddyListAdapter);

        mBuddyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) mBuddyListAdapter.getItem(position);
                Intent newIntent = new Intent(getContext(), BuddyProfileActivity.class);
                newIntent.putExtra(BuddyProfileActivity.USER_EXTRA_KEY, cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.USER_ID)));
                getContext().startActivity(newIntent);
            }
        });
        return view;
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.BUDDIES);
    }

    public static void refreshBuddyList() {
        if (mBuddyListView != null && context != null) {
            ltDb = new LearnTogetherContract(context);
            mBuddyListAdapter = new BuddyListAdapter(context, ltDb.getBuddies());
            mBuddyListView.setAdapter(mBuddyListAdapter);
        }
    }
}