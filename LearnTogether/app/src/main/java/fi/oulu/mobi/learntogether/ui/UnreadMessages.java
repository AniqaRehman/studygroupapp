package fi.oulu.mobi.learntogether.ui;

import android.app.NotificationManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.UnreadMessageAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class UnreadMessages extends Fragment {
    public static ListView unreadMessageList;
    public static LearnTogetherContract ltDb;
    static Context context;

    public UnreadMessages() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ltDb = new LearnTogetherContract(getContext());
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_unread_messages, container, false);
        setHasOptionsMenu(false);
        context = getContext();
        unreadMessageList = (ListView) view.findViewById(R.id.home_unread_messages);
        final UnreadMessageAdapter unreadMessageAdapter = new UnreadMessageAdapter(getContext(), ltDb.getUnreadMessages());
        unreadMessageList.setAdapter(unreadMessageAdapter);
        unreadMessageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) unreadMessageAdapter.getItem(position);
                Fragment groupFragment = new MyGroupFragment();
                // Insert the fragment by replacing any existing fragment
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.flContent, groupFragment).addToBackStack(Constants.HOME_FRAGMENT).commit();
                getActivity().setTitle(Constants.MY_GROUP);
                StateMachine.setCurrentState(StateMachine.States.COURSE_GROUPS);
            }
        });
        return view;

    }


    public static void refreshUnreadMessageList() {
        if (unreadMessageList != null) {
            ltDb = new LearnTogetherContract(context);
            unreadMessageList.setAdapter(new UnreadMessageAdapter(context, ltDb.getUnreadMessages()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //(getActivity()).setTitle(Constants.UNREAD_MESSAGES);
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.CHAT_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StateMachine.setCurrentState(StateMachine.States.UNREAD_MESSAGES);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
