package fi.oulu.mobi.learntogether.ui;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.ChatListAdapter;
import fi.oulu.mobi.learntogether.adapters.MeetingNoteAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Meeting;
import fi.oulu.mobi.learntogether.helpers.MeetingNote;
import fi.oulu.mobi.learntogether.helpers.Technology;

public class MeetingDetails extends AppCompatActivity {

    private long mMeetingId;
    private static boolean isPreviousMeeting;
    private String userId;
    private static LearnTogetherContract database;
    private Activity currentActivity;
    private static List<MeetingNote> notes;
    private Calendar now;
    private MeetingNoteAdapter mMeetingNotesAdapter;
    static private ListView meetingNotesList;
    static Meeting meeting;
    static Context context;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private ProgressDialog progress;

    public static final String GROUP_ID_EXTRA_KEY = "GRoupID";
    private final int RC_PICTURE_TAKEN = 1111;
    private final int RC_PERMISSIONS = 2222;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_details);
        currentActivity = this;
        mMeetingId = getIntent().getExtras().getLong(Constants.MEETING_ID_EXTRA_KEY);
        database = new LearnTogetherContract(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        userId = prefs.getString("userId", "Not Registered.");
        meetingNotesList = (ListView) findViewById(R.id.note_list);

        meeting = database.getMeeting(mMeetingId);

        TextView groupId = (TextView) findViewById(R.id.meeting_details_group);
        groupId.setText(database.getGroup(meeting.getGroupId()).getGroupName());

        TextView dateTime = (TextView) findViewById(R.id.meeting_detail_datetime);
        String dateTimeString = Technology.getTimeInFormat(meeting.getTimestamp());
        dateTime.setText(dateTimeString);

        TextView title = (TextView) findViewById(R.id.meeting_title);
        title.setText(meeting.getTitle());

        TextView desc = (TextView) findViewById(R.id.meeting_desc);
        desc.setText(meeting.getDescription());

        isPreviousMeeting = getIntent().getExtras().getBoolean(Constants.MEETING_IS_PREVIOUS_EXTRA);
        Button saveButton = (Button) findViewById(R.id.save_notes);
        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        Button textNoteButton = (Button) findViewById(R.id.text_note_button);
        Button picNoteButton = (Button) findViewById(R.id.pic_note_button);

        context = this;

        notes = database.getMeetingNotes(meeting.getServerMeetingId());


        if (isPreviousMeeting) {
            saveButton.setVisibility(View.INVISIBLE);
            cancelButton.setVisibility(View.INVISIBLE);
            textNoteButton.setVisibility(View.INVISIBLE);
            picNoteButton.setVisibility(View.INVISIBLE);
            if (meetingNotesList.getAdapter() == null) {
                mMeetingNotesAdapter = new MeetingNoteAdapter(currentActivity, notes, true);
                meetingNotesList.setAdapter(mMeetingNotesAdapter);

            }

        } else {
            if (meetingNotesList.getAdapter() == null) {
                mMeetingNotesAdapter = new MeetingNoteAdapter(currentActivity, notes, false);
                meetingNotesList.setAdapter(mMeetingNotesAdapter);

            }
            textNoteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(buttonClick);
                    LayoutInflater inflater = currentActivity.getLayoutInflater();
                    final View dialoglayout = inflater.inflate(R.layout.dialog_add_note, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(currentActivity);
                    builder.setView(dialoglayout);
                    builder.setTitle(Constants.ADD_NOTE);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText noteText = (EditText) dialoglayout.findViewById(R.id.text_note);
                            if (!noteText.getText().toString().equals("")) {
                                MeetingNote textNote = new MeetingNote();
                                textNote.setNoteType(Constants.NOTE_TYPE.TEXT.name());
                                textNote.setNote(noteText.getText().toString());
                                textNote.setUserId(userId);
                                textNote.setDeliveryStatus(Constants.NOTE_DELIVERY_STATUS.CREATED.name());
                                now = Calendar.getInstance();
                                textNote.setServerNoteId(userId + "_" + meeting.getServerMeetingId() + "_" + now.getTimeInMillis());
                                textNote.setServerMeetingId(meeting.getServerMeetingId());
                                textNote.setTimestamp(now.getTimeInMillis());
                                textNote.setDescription(meeting.getDescription());
                                notes.add(0, textNote);
                                    mMeetingNotesAdapter = new MeetingNoteAdapter(currentActivity, notes, false);
                                    meetingNotesList.setAdapter(mMeetingNotesAdapter);

                            }


                        }

                    });
                    final AlertDialog mAlertDialog = builder.create();
                    mAlertDialog.show();
                }
            });
            picNoteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MeetingDetails.this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSIONS);

                    } else {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, RC_PICTURE_TAKEN);

                    }

                }
            });
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progress = progress.show(context, "Meeting Notes", "Saving notes...");
                    for (MeetingNote note : notes) {
                        if (note.getNoteType().equals(Constants.NOTE_TYPE.PICTURE.name()) && note.getNote().equals(MeetingNoteAdapter.NOTE_CURRENT)) {
                            note.setNote(addImageToStorage(note.getImageBitMap(),
                                    meeting.getServerMeetingId(), note.getServerNoteId()));
                        }
                        saveToDatabase(note);

                    }

                    notes = database.getMeetingNotes(meeting.getServerMeetingId());
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.note_saved),
                            Toast.LENGTH_SHORT).show();
                    refreshList(meeting.getServerMeetingId());
                    progress.dismiss();
                }
            });

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }


    }

    private void saveToDatabase(MeetingNote meetingNote) {
        //Adding data to local database
        LearnTogetherContract ltDb = new LearnTogetherContract(this);
        try {
            if (ltDb.addMeetingNote(meetingNote)) {

            }
        } catch (SQLiteException se) {
            se.printStackTrace();
            Toast.makeText(
                    getApplicationContext(),
                    getResources().getString(R.string.error_contact_administrator),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PICTURE_TAKEN && resultCode == RESULT_OK) {

            MeetingNote picNote = new MeetingNote();
            picNote.setNoteType(Constants.NOTE_TYPE.PICTURE.name());
            picNote.setNote(MeetingNoteAdapter.NOTE_CURRENT);
            picNote.setUserId(userId);
            picNote.setDeliveryStatus(Constants.NOTE_DELIVERY_STATUS.CREATED.name());
            now= Calendar.getInstance();
            picNote.setServerNoteId(userId + "_" + meeting.getServerMeetingId() + "_" + now.getTimeInMillis());
            picNote.setServerMeetingId(meeting.getServerMeetingId());
            picNote.setTimestamp(now.getTimeInMillis());
            picNote.setDescription(meeting.getDescription());
            picNote.setImageBitMap((Bitmap) data.getExtras().get("data"));
            notes.add(0, picNote);
            mMeetingNotesAdapter = new MeetingNoteAdapter(currentActivity, notes, false);
            meetingNotesList.setAdapter(mMeetingNotesAdapter);
        }
        if (requestCode == RC_PERMISSIONS && resultCode == RESULT_OK) {
            Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePicture, RC_PICTURE_TAKEN);
        }

    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static void refreshList(String serverMeetingId) {
        if (context != null && meeting != null && meeting.getServerMeetingId().equals(serverMeetingId)) {
            notes = database.getMeetingNotes(meeting.getServerMeetingId());
            if (isPreviousMeeting) {
                MeetingNoteAdapter mMeetingNotesAdapter = new MeetingNoteAdapter(context, notes , true);
                meetingNotesList.setAdapter(mMeetingNotesAdapter);
            } else {
                MeetingNoteAdapter mMeetingNotesAdapter = new MeetingNoteAdapter(context, notes, false);
                meetingNotesList.setAdapter(mMeetingNotesAdapter);
            }
        }
    }

    private String addImageToStorage(Bitmap img, String meetingId, String noteId) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOutputStream = null;
        File file = new File(path + "/" + Constants.FOLDER_NAME + "/MeetingNotes/");
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(path + "/" + Constants.FOLDER_NAME + "/MeetingNotes/" + meetingId + "/");
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(path + "/" + Constants.FOLDER_NAME + "/MeetingNotes/" + meetingId + "/", noteId + ".jpg");
        try {
            fOutputStream = new FileOutputStream(file);
            img.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
            fOutputStream.flush();
            fOutputStream.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            return file.getPath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.MEETING_DETAIL);
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.NOTE_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
