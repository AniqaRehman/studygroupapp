package fi.oulu.mobi.learntogether.ui;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fi.oulu.mobi.learntogether.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragmentCompat {
    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_messaging);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SwitchPreferenceCompat mSwitchPreference = (SwitchPreferenceCompat) getPreferenceManager().findPreference("all_notification");
        final SwitchPreferenceCompat mChatPreference = (SwitchPreferenceCompat) getPreferenceManager().findPreference("chat_notification");
        final SwitchPreferenceCompat mMeetingPreference = (SwitchPreferenceCompat) getPreferenceManager().findPreference("meetings_notification");
        final SwitchPreferenceCompat mNotePreference = (SwitchPreferenceCompat) getPreferenceManager().findPreference("notes_notification");
        final SwitchPreferenceCompat mBuddyPreference = (SwitchPreferenceCompat) getPreferenceManager().findPreference("buddy_request_notification");

        if(preferences.getBoolean("all_notification", false)){
            mChatPreference.setEnabled(false);
            mMeetingPreference.setEnabled(false);
            mNotePreference.setEnabled(false);
            mBuddyPreference.setEnabled(false);
        }

        mSwitchPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean value = (boolean) newValue;
                if (value) {
                    mChatPreference.setEnabled(false);
                    mMeetingPreference.setEnabled(false);
                    mNotePreference.setEnabled(false);
                    mBuddyPreference.setEnabled(false);
                } else {
                    mChatPreference.setEnabled(true);
                    mMeetingPreference.setEnabled(true);
                    mNotePreference.setEnabled(true);
                    mBuddyPreference.setEnabled(true);
                }
                return true;
            }
        });
    }



    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {


    }
}
