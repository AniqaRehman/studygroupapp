package fi.oulu.mobi.learntogether.helpers;

import android.content.Context;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import java.io.InputStream;

/**
 * Created by raniq on 4/19/2017.
 */

public class AWSS3Wrapper {
    private static AmazonS3 s3Client = null;
    private static TransferUtility transferUtility;
    private static String bucketName = "mobisocial.learntogether";

    private final static String FOLDER_SUFFIX = "/";
    private final static String TAG = "AWSS3Wrapper";

    public AWSS3Wrapper(Context context) {
        if (s3Client == null) {
            CognitoCachingCredentialsProvider cognitoProvider = new CognitoCachingCredentialsProvider(
                    context,
                    Constants.COGNITO_ACOUNT_ID,
                    Constants.COGNITO_POOL_ID,
                    Constants.COGNITO_ROLE_ARN,
                    Constants.COGNITO_ROLE_ARN,
                    Regions.US_WEST_2);

            s3Client = new AmazonS3Client(cognitoProvider);
            s3Client.setEndpoint("s3-us-west-2.amazonaws.com");
        }
    }

    public String uploadFileOnS3(String foldername, String fileName, String filePath) {
        String url = "";
        try {
            PutObjectRequest por = new PutObjectRequest(bucketName, foldername + FOLDER_SUFFIX + fileName, new java.io.File(filePath));
            s3Client.putObject(por);
            url = foldername + "/" + fileName;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (AmazonServiceException ase) {
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

        } catch (AmazonClientException ace) {
            System.out.println("Error Message: " + ace.getMessage());
        }
        return url;
    }

    public InputStream getFileFromS3(String url) {
        InputStream ioStream = null;
        try {
            S3Object s3object = s3Client.getObject(new GetObjectRequest(bucketName, url));
            ioStream = s3object.getObjectContent();
        } catch (AmazonServiceException ase) {
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

        } catch (AmazonClientException ace) {
            System.out.println("Error Message: " + ace.getMessage());
        }
        return ioStream;
    }

    public boolean ifFileexists(String path) {
        try {
            s3Client.getObjectMetadata(bucketName, path);
        } catch (AmazonServiceException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
