package fi.oulu.mobi.learntogether.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSS3Wrapper;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Technology;

import static fi.oulu.mobi.learntogether.helpers.Constants.FOLDER_DELIMITER;
import static fi.oulu.mobi.learntogether.helpers.Constants.NEW_HEIGHT;
import static fi.oulu.mobi.learntogether.helpers.Constants.NEW_WIDTH;
import static fi.oulu.mobi.learntogether.helpers.Constants.PI_NAME;
import static fi.oulu.mobi.learntogether.helpers.Constants.PROFILE_FOLDER_NAME;

/**
 * Created by raniq on 4/1/2017.
 */

public class UserProfile extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    private static final int SELECT_PICTURE = 2;
    public static final String PI_JPG = "/PI.jpg";
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 2;
    FloatingActionButton imageButton;
    ImageView picture;
    EditText firstname;
    EditText lastname;
    EditText studentId;
    //   EditText dateofBirth;
    EditText country;
    EditText phoneNumber;
    EditText emailId;
    EditText facebookId;
    EditText skypeId;

    Button saveButton;

    String profilePicture = "";
    String location = "";

    SharedPreferences savedValues;

    public static String TAG = "User Profile";

    private Uri selectedImageUri;
    private Bitmap img;
    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    LearnTogetherContract ltDb;
    fi.oulu.mobi.learntogether.helpers.UserProfile userProfile;
    private Context context;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        initialize();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

    }

    public void initialize() {
        imageButton = (FloatingActionButton) findViewById(R.id.add_picture);
        imageButton.setOnClickListener(this);
        firstname = (EditText) findViewById(R.id.first_name);
        lastname = (EditText) findViewById(R.id.last_name);
        phoneNumber = (EditText) findViewById(R.id.phone_number);
        studentId = (EditText) findViewById(R.id.student_id);
        //dateofBirth = (EditText) findViewById(R.id.date_of_birth);
        skypeId = (EditText) findViewById(R.id.skype_id);
        facebookId = (EditText) findViewById(R.id.fb_id);
        emailId = (EditText) findViewById(R.id.email_id);
        country = (EditText) findViewById(R.id.country);
        picture = (ImageView) findViewById(R.id.picture);

        saveButton = (Button) findViewById(R.id.save_user_profile);
        saveButton.setOnClickListener(this);
        savedValues = PreferenceManager
                .getDefaultSharedPreferences(this);

        ltDb = new LearnTogetherContract(UserProfile.this);

        if (savedValues.contains("userId") && !savedValues.getString("userId", "").isEmpty()) {
            userProfile = ltDb.getUserProfile(savedValues.getString("userId", ""));
            firstname.setText(userProfile.getFirstName());
            lastname.setText(userProfile.getLastName());
            phoneNumber.setText(userProfile.getContactNumber());
            studentId.setText("" + userProfile.getStudentId());
            // dateofBirth.setText("");
            skypeId.setText(userProfile.getSkypeId());
            facebookId.setText(userProfile.getFacebookId());
            emailId.setText(userProfile.getEmail());
            country.setText(userProfile.getCountry());

            String path = Environment.getExternalStorageDirectory().toString();
            path = path + FOLDER_DELIMITER + Constants.FOLDER_NAME + FOLDER_DELIMITER + PROFILE_FOLDER_NAME +
                    FOLDER_DELIMITER + PI_NAME;
            if (Technology.getBitmap(path) != null)
                picture.setImageBitmap(Technology.getBitmap(path));
        }
        context = this;
    }

    private void registerUser(final String studentID, final String profile) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {

                if (userProfile == null) {
                    // Get FCM token
                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    //Log the token
                    Log.d(TAG, "Refreshed token: " + refreshedToken);
                    // Registering with amazon cloud
                    AWSSNSWrapper sns = new AWSSNSWrapper(
                            getApplicationContext());
                    String arn = sns.getARNFromRegID(studentID, refreshedToken);

                    String profile_url = "";
                    if (profile != null && !profile.isEmpty()) {
                        Log.d(TAG, "url: " + profile);
                        AWSS3Wrapper s3 = new AWSS3Wrapper(getApplicationContext());
                        profile_url = s3.uploadFileOnS3(studentID, "PI.jpg", profile);
                    }
                    //Log the token
                    Log.d(TAG, "ARN: " + arn);
                    return refreshedToken + "," + arn;
                } else {
                    String profile_url = "";
                    if (profile != null && !profile.isEmpty()) {
                        Log.d(TAG, "url: " + profile);
                        AWSS3Wrapper s3 = new AWSS3Wrapper(getApplicationContext());
                        profile_url = s3.uploadFileOnS3(studentID, "PI.jpg", profile);
                    }

                    return userProfile.getGcmId() + "," + userProfile.getArn();
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                progress.dismiss();
                String temp[] = ((String) o).split("\\,");
                if (null != temp && temp.length == 2) {
                    String refreshedToken = temp[0];
                    String arn = temp[1];
                    if (arn != null && !arn.isEmpty()) {
                        location = currentLatitude + ":" + currentLongitude;
                        fi.oulu.mobi.learntogether.helpers.UserProfile userProfileObject = new fi.oulu.mobi.learntogether.helpers.UserProfile("",
                                Integer.parseInt(studentId.getText().toString()), firstname.getText().toString(),
                                lastname.getText().toString(), emailId.getText().toString(),
                                facebookId.getText().toString(), skypeId.getText().toString(),
                                "", country.getText().toString(),
                                phoneNumber.getText().toString(), refreshedToken, arn, studentId.getText().toString() + PI_JPG,
                                location, System.currentTimeMillis());

                        // Adding data to firebase database
                        FirebaseDatabaseManager fcmDb = new FirebaseDatabaseManager();
                        if (savedValues.contains("userId") && !savedValues.getString("userId", "").isEmpty()) {
                            userProfileObject.setUserId(fcmDb.updateUserProfile(userProfileObject, savedValues.getString("userId", "")));
                            if (ltDb.updateUserProfile(userProfileObject)) {
                                Toast.makeText(
                                        getApplicationContext(),
                                        getResources().getString(R.string.profile_updated),
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } else {
                            userProfileObject.setUserId(fcmDb.writeUserProfile(userProfileObject));
                            //Adding data to local database
                            try {
                                if (ltDb.addUserProfile(userProfileObject)) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            getResources().getString(R.string.registered),
                                            Toast.LENGTH_SHORT).show();

                                    SharedPreferences.Editor editor = savedValues.edit();
                                    editor.putString("userId", userProfileObject.getUserId());
                                    editor.putBoolean("register", true);
                                    editor.commit();
                                    Intent newIntent = new Intent(UserProfile.this, MainActivity.class);
                                    startActivity(newIntent);
                                    finish();
                                } else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            getResources().getString(R.string.error_contact_administrator),
                                            Toast.LENGTH_LONG).show();
                                }
                            } catch (SQLiteException se) {
                                se.printStackTrace();
                                Toast.makeText(
                                        getApplicationContext(),
                                        getResources().getString(R.string.error_contact_administrator),
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(
                                getApplicationContext(),
                                getResources().getString(R.string.error_contact_administrator),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.error_contact_administrator),
                            Toast.LENGTH_LONG).show();
                }

                saveButton.setVisibility(View.VISIBLE);
                imageButton.setVisibility(View.VISIBLE);
            }
        }.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_user_profile:
                //Checking network availability
                ConnectivityManager connMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = connMan.getActiveNetworkInfo();

                if (netInfo != null && netInfo.isConnected()) {
                    if (!firstname.getText().toString().isEmpty() && !studentId.getText().toString().isEmpty() && firstname.getText().toString() != null && studentId.getText().toString() != null) {
                        progress = progress.show(this, "User Profile", "Registering");
                        registerUser(studentId.getText().toString(), profilePicture);
                        saveButton.setVisibility(View.INVISIBLE);
                        imageButton.setVisibility(View.INVISIBLE);
                    } else {
                        Toast.makeText(
                                getApplicationContext(),
                                getResources().getString(R.string.required_fields_check),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.no_stable_network),
                            Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.add_picture:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_STORAGE);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, null);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_picture)),
                            SELECT_PICTURE);
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_PICK, null);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_picture)),
                            SELECT_PICTURE);
                } else {
                }
                break;
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {

        Bitmap image = null;
        try {
            ParcelFileDescriptor parcelFileDescriptor = getContentResolver()
                    .openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor
                    .getFileDescriptor();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            options.inSampleSize = calculateInSampleSize(options, 200, 200);
            options.inJustDecodeBounds = false;
            image = BitmapFactory.decodeFileDescriptor(fileDescriptor, null,
                    options);

            parcelFileDescriptor.close();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /*
     * Gets the file path of the given Uri.
     */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                try {
                    selectedImageUri = data.getData();
                    img = getBitmapFromUri(selectedImageUri);
                    if (img != null) {
                        img = resizeImage(img);
                        addImageToStorage(img);
                        picture.setImageBitmap(img);
                        profilePicture = getPath(selectedImageUri);
                    } else if (img == null) {
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.choose_image), Toast.LENGTH_SHORT).show();
                    }
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Log.d(TAG,
                            e.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG,
                            e.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG,
                            e.toString());
                }
            }
        }
    }

    private String addImageToStorage(Bitmap img) {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOutputStream = null;
        File file = new File(path + "/" + Constants.FOLDER_NAME + "/Profile/");
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(path + "/" + Constants.FOLDER_NAME + "/Profile/", "PI.jpg");
        try {
            fOutputStream = new FileOutputStream(file);
            img.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
            fOutputStream.flush();
            fOutputStream.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            return file.getPath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
        }
        return "";
    }

    private Bitmap resizeImage(Bitmap img) {
        return Bitmap.createScaledBitmap(
                img, NEW_WIDTH, NEW_HEIGHT, false);
    }


    /**
     * If connected get lat and long
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            } else {
                //If everything went fine lets get latitude and longitude
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();

                Log.d(TAG, currentLatitude + " WORKS " + currentLongitude + "");
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        Log.d(TAG, currentLatitude + " WORKS " + currentLongitude + "");
    }


}
