package fi.oulu.mobi.learntogether.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSS3Wrapper;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.BuddyRequest;
import fi.oulu.mobi.learntogether.helpers.Constants;

import static android.nfc.NdefRecord.createMime;

public class NFCBuddyActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback{

    NfcAdapter mNfcAdapter;
    private String mUserId;
    static LearnTogetherContract learnTogetherContract;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfcbuddy);
        learnTogetherContract = new LearnTogetherContract(this);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        mUserId = prefs.getString("userId", "Not Registered.");
        // Check for available NFC Adapter
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        // Register callback
        mNfcAdapter.setNdefPushMessageCallback(this, this);

    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {

        NdefMessage msg = new NdefMessage(
                new NdefRecord[] { createMime(
                        "application/vnd.com.example.android.beam", mUserId.getBytes())
                        /**
                         * The Android Application Record (AAR) is commented out. When a device
                         * receives a push with an AAR in it, the application specified in the AAR
                         * is guaranteed to run. The AAR overrides the tag dispatch system.
                         * You can add it back in to guarantee that this
                         * activity starts when receiving a beamed message. For now, this code
                         * uses the tag dispatch system.
                        */
                        ,NdefRecord.createApplicationRecord("fi.oulu.mobi.learntogether")
                });
        return msg;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    /**
     * Parses the NDEF Message from the intent and prints to the TextView
     */
    void processIntent(Intent intent) {
       // textView = (TextView) findViewById(R.id.textView);
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        if (rawMsgs != null) {
            NdefMessage[] messages = new NdefMessage[rawMsgs.length];
            for (int i = 0; i < rawMsgs.length; i++) {
                messages[i] = (NdefMessage) rawMsgs[i];
            }
        }
        String recievedUserId = new String(msg.getRecords()[0].getPayload());
        //Store in db get buddy data

        getBuddyData(recievedUserId);
        // record 0 contains the MIME type, record 1 is the AAR, if present
       // textView.setText(new String(msg.getRecords()[0].getPayload()));
    }


    private void sendMessage(final String messagePacket, final String userARN, final Context context) {
        new AsyncTask<Object, Object, Object>() {
            protected Object doInBackground(Object... params) {
                try {
                    AWSSNSWrapper sns = new AWSSNSWrapper(context);

                    sns.publish(userARN, messagePacket);
                     return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean result = (boolean) o;
            }
        }.execute();
    }

    private void getBuddyData(String userId) {
        FirebaseDatabaseManager fbDb = new FirebaseDatabaseManager();
        final LearnTogetherContract ltDb = new LearnTogetherContract(getApplicationContext());
        fbDb.getUser(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Buddy buddy = dataSnapshot.getValue(Buddy.class);
                            buddy.setUserId(dataSnapshot.getKey());

                            saveProfilePicture(buddy, getApplicationContext());

                            Map<String, String> message = new HashMap<>();
                            message.put(Constants.NFC_BUDDY_MESSAGE, PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("userId",""));
                            Gson gson = new Gson();
                            sendMessage(gson.toJson(message), buddy.getArn(), getApplicationContext());

                            ltDb.addBuddies(buddy);
                            if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                                MyBuddies.runOnUI(new Runnable() {
                                    public void run() {
                                        try {
                                            MyBuddies
                                                    .refreshBuddyList();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // read query is cancelled.
                    }
                });

    }

    private String saveProfilePicture(final Buddy buddy, final Context context) {
        new AsyncTask<Object, Object, Object>() {

            protected Object doInBackground(Object... params) {
                AWSS3Wrapper s3 = new AWSS3Wrapper(context);
                if (s3.ifFileexists(buddy.getProfilePicture())) {
                    String IMAGE_DIR_PATH = Environment.getExternalStorageDirectory()
                            + File.separator + Constants.FOLDER_NAME + File.separator + Constants.PROFILE_FOLDER_NAME
                            + File.separator + buddy.getUserId() + "/";
                    File imageDirPath = new File(IMAGE_DIR_PATH);

                    if (!imageDirPath.exists()) {
                        imageDirPath.mkdirs();
                    }
                    File file = new File(imageDirPath, Constants.PI_NAME);
                    if (file.exists()) {
                        file.delete();
                    }

                    FileOutputStream out = null;
                    try {
                        InputStream object = s3.getFileFromS3(buddy.getProfilePicture());
                        if (object != null) {
                            out = new FileOutputStream(file);
                            byte[] b = new byte[2048];
                            int length;
                            while ((length = object.read(b)) != -1) {
                                out.write(b, 0, length);
                            }
                            object.close();
                            out.flush();
                            out.close();
                        }
                        return file.getAbsolutePath();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        return "";
                    } catch (IOException e) {
                        e.printStackTrace();
                        return "";
                    }
                }
                return "";
            }

            @Override
            protected void onPostExecute(Object o) {
                String result = (String) o;
                if (!result.isEmpty()) {
                    LearnTogetherContract learnTogetherContract = new LearnTogetherContract(context);
                    learnTogetherContract.updateBuddyProfile(buddy.getUserId(), result);
                    if (StateMachine.getCurrentState() == StateMachine.States.BUDDIES)
                        MyBuddies.runOnUI(new Runnable() {
                            public void run() {
                                try {
                                    MyBuddies
                                            .refreshBuddyList();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                } else {

                }
            }
        }.execute();
        return "";
    }



    private String createMessagePacket(BuddyRequest buddyRequest) {
        String message = "";
        Map<String, String> messageMap = new HashMap<>();
        messageMap.put(LearnTogetherContract.BuddyRequestEntry.TABLE_NAME, "" + true);
        messageMap.put(LearnTogetherContract.BuddyRequestEntry.USER_ID, mUserId);
        messageMap.put(LearnTogetherContract.BuddyRequestEntry.TIMESTAMP, "" + buddyRequest.getTimestamp());
        messageMap.put(LearnTogetherContract.BuddyRequestEntry.RECEVER_ID, "" + buddyRequest.getUserId());

        Gson gson = new Gson();
        message = gson.toJson(messageMap);

        return message;
    }


}
