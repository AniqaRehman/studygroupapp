package fi.oulu.mobi.learntogether.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

/**
 * Created by raniq on 4/1/2017.
 */

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * helper methods.
 */
public class AWSMessageReceivingService extends IntentService {

    public static final String TAG = "AWS Messaging Service";

    public AWSMessageReceivingService() {
        super("AWSMessageReceivingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
        }
    }
}
