package fi.oulu.mobi.learntogether.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.ui.ChatFragment;
import fi.oulu.mobi.learntogether.ui.CourseDetailFragment;
import fi.oulu.mobi.learntogether.ui.MainActivity;
import fi.oulu.mobi.learntogether.ui.MeetingsFragment;
import fi.oulu.mobi.learntogether.ui.MyCourses;
import fi.oulu.mobi.learntogether.ui.PersonalGroupFragment;
import fi.oulu.mobi.learntogether.ui.StateMachine;

/**
 * Created by raniq on 4/2/2017.
 */

public class PersonalGroupAdapter extends CursorAdapter {
    LearnTogetherContract learnTogetherContract;
    private static ProgressDialog progress;
    private Context context;

    public PersonalGroupAdapter(Context context, Cursor c) {
        super(context, c);
        this.context = context;
        learnTogetherContract = new LearnTogetherContract(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.personal_group_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView groupNameText = (TextView) view.findViewById(R.id.personal_Group_Name);
        final String groupname = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_NAME));
        final String grouServerId = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_SERVER_ID));
        groupNameText.setText(groupname);
        final FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
        groupNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment courseDetailFragment = CourseDetailFragment.newInstance(grouServerId, Constants.GROUP_TYPES.PERSONAL.name());
                // Insert the fragment by replacing any existing fragment
                fragmentManager.beginTransaction().replace(R.id.flContent, courseDetailFragment).addToBackStack(Constants.MY_GROUP).commit();
                ((MainActivity) context).setTitle(Constants.COURSE_DETAIL);
                StateMachine.setCurrentState(StateMachine.States.GROUP_DETAIL);
            }
        });

        final String groupId = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_SERVER_ID));
        final String groupNameString = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_NAME));
        final String mGroupArn = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.GroupEntry.GROUP_ARN));

        ImageButton meetingButton = (ImageButton) view.findViewById(R.id.group_meetings);
        meetingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment meetingsFragment = MeetingsFragment.newInstance(groupNameString, groupId);
                // Insert the fragment by replacing any existing fragment
                StateMachine.setCurrentState(StateMachine.States.MEETING_SCREEN);

                fragmentManager.beginTransaction().replace(R.id.flContent, meetingsFragment).addToBackStack(Constants.MY_GROUP).commit();
                ((MainActivity) context).setTitle(Constants.MEETING);

            }
        });

        ImageButton chatButton = (ImageButton) view.findViewById(R.id.chat_group);
        chatButton.setOnClickListener(new PersonalGroupAdapter.MyOnClickListener(context, groupId));

        LinearLayout unreadLayout = (LinearLayout) view.findViewById(R.id.unreadLayout);
        TextView unreadCount = (TextView) view.findViewById(R.id.unreadCount);
        int count = learnTogetherContract.getUnreadMessageCount(groupId);
        if (count == 0) {
            unreadLayout.setVisibility(View.GONE);
        } else {
            unreadLayout.setVisibility(View.VISIBLE);
            unreadCount.setText("" + count);
        }

        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.delete_group);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progress = progress.show(context, "My Groups", "Deleting Group...");
                Map<String, String> messageMap = new HashMap<>();
                messageMap.put(Constants.REFRESH_MEMBERS, "" + true);
                messageMap.put(LearnTogetherContract.GroupMemberEntry.GROUP_ID, groupId);

                Gson gson = new Gson();
                final String message = gson.toJson(messageMap);

                v.setEnabled(false);
                final FirebaseDatabaseManager fbDb = new FirebaseDatabaseManager();
                fbDb.getGroupMembers(groupId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            if (postSnapshot.getValue() != null) {
                                GroupMember groupMember = postSnapshot.getValue(GroupMember.class);
                                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                                if (groupMember.getUserId().equals(preferences.getString("userId", ""))) {
                                    postSnapshot.getRef().removeValue();
                                    sendMessage(message, v, learnTogetherContract.getSubscriptionId(preferences.getString("userId", ""), groupId), mGroupArn, mContext, groupId);
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

    }

    private static class MyOnClickListener implements View.OnClickListener {
        Context mContext;
        String mCode;

        public MyOnClickListener(Context context, String groupId) {
            mContext = context;
            mCode = groupId;
        }

        @Override
        public void onClick(View v) {
            if (mContext instanceof MainActivity) {
                MainActivity mainActivity = (MainActivity) mContext;
                FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();

                switch (v.getId()) {
                    case R.id.chat_group:
                        Fragment chatsFragment = ChatFragment.newInstance(mCode);
                        // Insert the fragment by replacing any existing fragment
                        fragmentManager.beginTransaction().replace(R.id.flContent, chatsFragment).addToBackStack(Constants.MY_GROUP).commit();
                        mainActivity.setTitle(Constants.CHAT_LIST);
                        StateMachine.setCurrentState(StateMachine.States.CHAT_SCREEN);
                        break;
                }
            }
        }
    }

    private static void sendMessage(final String messagePacket, final View v, final String subscriptionId, final String mGroupArn, final Context context, final String groupId) {
        new AsyncTask<Object, Object, Object>() {
            protected Object doInBackground(Object... params) {
                try {
                    AWSSNSWrapper sns = new AWSSNSWrapper(context);
                    sns.unSubscribeMember(subscriptionId);
                    sns.publish(mGroupArn, messagePacket);
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean result = (boolean) o;
                progress.dismiss();
                v.setEnabled(true);
                if(result) {

                    LearnTogetherContract learnTogetherContract = new LearnTogetherContract(context);
                    learnTogetherContract.clearGroupMembers(groupId);
                    learnTogetherContract.deleteGroup(groupId);
                }
                PersonalGroupFragment.refreshGroupList();
            }
        }.execute();
    }
}