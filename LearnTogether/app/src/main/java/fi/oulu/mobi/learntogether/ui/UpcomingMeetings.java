package fi.oulu.mobi.learntogether.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.Calendar;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.MeetingsListAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class UpcomingMeetings extends Fragment {

    public static final String MEETING_GROUP_EXTRA = "MeetingGroup";
    private SQLiteManager database;
    private String userId;
    public static LearnTogetherContract ltDb;
    static Context context;

    public UpcomingMeetings() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new SQLiteManager(getContext());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        userId = prefs.getString("userId", "Not Registered.");
        ltDb = new LearnTogetherContract(getContext());
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_meetings, container, false);
        setHasOptionsMenu(false);
        context = getContext();
        ListView upcomingMeetingsList = (ListView) view.findViewById(R.id.home_meeting_list);
        Calendar now = Calendar.getInstance();
        Cursor cursor = database.getReadableDatabase()
                .query(LearnTogetherContract.Meeting.TABLE_NAME, null, ""
                                + LearnTogetherContract.Meeting.TIMESTAMP + ">=" + now.getTimeInMillis()
                        , null, null, null
                        , LearnTogetherContract.Meeting.TIMESTAMP + " ASC");

        MeetingsListAdapter meetingsListAdapter = new MeetingsListAdapter(getContext(), cursor, true, false);
        upcomingMeetingsList.setAdapter(meetingsListAdapter);

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        //(getActivity()).setTitle(Constants.UPCOMING_MEETINGS);
        StateMachine.setCurrentState(StateMachine.States.UPCOMING_MEETINGS);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
