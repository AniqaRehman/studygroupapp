package fi.oulu.mobi.learntogether.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.helpers.Constants;

/**
 * Created by raniq on 4/1/2017.
 */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class MyGroupFragment extends Fragment {


    public MyGroupFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_group, container, false);
        setHasOptionsMenu(false);
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Course Groups"));
        tabLayout.addTab(tabLayout.newTab().setText("Personal Groups"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager mViewPager = (ViewPager) rootView.findViewById(R.id.pager);

        final GroupFragmentAdapter adapter = new GroupFragmentAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public static class GroupFragmentAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public GroupFragmentAdapter(FragmentManager fm, int numOfTabs) {
            super(fm);
            this.mNumOfTabs = numOfTabs;
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    StateMachine.setCurrentState(StateMachine.States.COURSE_GROUPS);
                    return new CourseGroupFragment();
                case 1:
                    StateMachine.setCurrentState(StateMachine.States.PERSONAL_GROUPS);
                    return new PersonalGroupFragment();
                default:
                    return new PersonalGroupFragment();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Course Groups";
                case 1:
                    return "Personal Groups";
                default:
                    return "Personal Groups";
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        (getActivity()).setTitle(Constants.MY_GROUP);
        StateMachine.setCurrentState(StateMachine.States.GROUPS);

    }
}
