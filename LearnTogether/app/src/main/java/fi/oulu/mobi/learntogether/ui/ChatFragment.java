package fi.oulu.mobi.learntogether.ui;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.ChatListAdapter;
import fi.oulu.mobi.learntogether.adapters.MeetingsListAdapter;
import fi.oulu.mobi.learntogether.database.FirebaseDatabaseManager;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.AWSSNSWrapper;
import fi.oulu.mobi.learntogether.helpers.Buddy;
import fi.oulu.mobi.learntogether.helpers.Chat;
import fi.oulu.mobi.learntogether.helpers.Constants;
import fi.oulu.mobi.learntogether.helpers.Group;
import fi.oulu.mobi.learntogether.helpers.GroupMember;
import fi.oulu.mobi.learntogether.helpers.Technology;

/**
 * Created by raniq on 4/1/2017.
 */

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class ChatFragment extends Fragment implements View.OnClickListener {

    // Customize parameter argument names
    private static final String ARG_GROUP_ID = "group_id";
    // Customize parameters
    private static String mGroupId = "";

    private Group group;
    private static LearnTogetherContract ltDb;
    EditText messageText;
    ImageButton sendButton;
    static ListView mChatList;
    static List<Chat> chatListFromDb;

    static Context context;

    public static boolean positionChange = false;

    private String userId;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ChatFragment() {
    }

    //  Customize parameter initialization
    @SuppressWarnings("unused")
    public static ChatFragment newInstance(String groupId) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_GROUP_ID, groupId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mGroupId = getArguments().getString(ARG_GROUP_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
        setHasOptionsMenu(false);
        initialize(view);
        context = getContext();
        positionChange = true;
        refreshView();

        return view;
    }

    private void initialize(View view) {
        ltDb = new LearnTogetherContract(getContext());
        group = ltDb.getGroup(mGroupId);

        messageText = (EditText) view.findViewById(R.id.messageText);
        sendButton = (ImageButton) view.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(this);
        mChatList = (ListView) view.findViewById(R.id.list);
        chatListFromDb = new ArrayList<Chat>();
        mChatList.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity().getApplication());
        userId = preferences.getString("userId", "");

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendButton:
                Chat newChat = new Chat();
                newChat.setMessage(messageText.getText().toString());
                newChat.setTimestamp(System.currentTimeMillis());
                newChat.setMessageType(Constants.MESSAGE_TYPE.TEXT.name());
                newChat.setUserId(userId);
                newChat.setGroupId(mGroupId);
                newChat.setMessageStatus(Constants.MESSAGE_STATUS.SENT.name());
                newChat.setServerMessageId(System.currentTimeMillis() + "," + userId + "," + mGroupId);
                ltDb.saveChat(newChat);

                sendMessage(createMessagePacket(newChat));

                messageText.setText("");
                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(
                        getActivity().getCurrentFocus().getWindowToken(), 0);

                positionChange = true;
                refreshView();
                break;
        }
    }

    private void sendMessage(final String messagePacket) {
        new AsyncTask<Object, Object, Object>() {
            protected Object doInBackground(Object... params) {
                try {
                    AWSSNSWrapper sns = new AWSSNSWrapper(getContext());

                    sns.publish(group.getGroupARN(), messagePacket);

                    Gson gson = new Gson();
                    Map<String, String> map = new HashMap<>();
                    map = (Map<String, String>) gson.fromJson(messagePacket, map.getClass());
                    ltDb.updateMessageStatus(map.get(LearnTogetherContract.ChatEntry.SERVER_MESSAGE_ID), Constants.MESSAGE_STATUS.DELIVERED.name());
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Object o) {
                boolean result = (boolean) o;
            }
        }.execute();
    }

    private String createMessagePacket(Chat newChat) {
        String message = "";
        Map<String, String> messageMap = new HashMap<>();
        messageMap.put(LearnTogetherContract.ChatEntry.GROUP_ID, newChat.getGroupId());
        messageMap.put(LearnTogetherContract.ChatEntry.TIMESTAMP, "" + newChat.getTimestamp());
        messageMap.put(LearnTogetherContract.ChatEntry.SERVER_MESSAGE_ID, newChat.getServerMessageId());
        messageMap.put(LearnTogetherContract.ChatEntry.MESSAGE, newChat.getMessage());
        messageMap.put(LearnTogetherContract.ChatEntry.MESSAGE_TYPE, newChat.getMessageType());
        messageMap.put(LearnTogetherContract.ChatEntry.USER_ID, newChat.getUserId());
        Gson gson = new Gson();
        message = gson.toJson(messageMap);

        return message;
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static void refreshView() {
        if (mChatList != null && ltDb != null && context != null) {
            ltDb.updateMessagesStatus(mGroupId, Constants.MESSAGE_STATUS.READ.name());
            chatListFromDb = ltDb.getChatList(mGroupId);
            if (chatListFromDb.size() > 0) {
                List<Chat> temporary = new ArrayList<Chat>();
                String[] checkDate = Technology.getTimeInFormat(chatListFromDb.get(0).getTimestamp())
                        .split("\\s");
                for (int loop = 0; loop < chatListFromDb.size(); loop++) {
                    String[] test = Technology.getTimeInFormat(chatListFromDb.get(loop).getTimestamp())
                            .split("\\s");
                    if (!test[0].equals(checkDate[0]) || loop == 0) {
                        Chat temp = new Chat();
                        temp.setId(-1);
                        temp.setTimestamp(chatListFromDb.get(loop)
                                .getTimestamp());
                        temporary.add(temp);
                        checkDate = Technology.getTimeInFormat(chatListFromDb.get(loop).getTimestamp())
                                .split("\\s");
                    }
                    temporary.add(chatListFromDb.get(loop));
                }
                chatListFromDb = temporary;
            }
            if (mChatList.getAdapter() == null) {
                final ChatListAdapter adapter = new ChatListAdapter(context, chatListFromDb);
                mChatList.setAdapter(adapter);
                if (positionChange) {
                    mChatList.clearFocus();
                    mChatList.post(new Runnable() {
                        @Override
                        public void run() {
                            mChatList.setSelection(adapter.getCount() - 1);
                        }
                    });
                } else {
                    int lastViewedPosition = mChatList.getFirstVisiblePosition();

                    //get offset of first visible view
                    View v = mChatList.getChildAt(0);
                    int topOffset = (v == null) ? 0 : v.getTop();
                    mChatList.setSelectionFromTop(lastViewedPosition, topOffset);
                }
            } else {
                ((ChatListAdapter) (mChatList
                        .getAdapter()))
                        .refill(chatListFromDb);
                if (positionChange) {
                    mChatList.clearFocus();
                    mChatList.post(new Runnable() {
                        @Override
                        public void run() {
                            mChatList.setSelection(((ChatListAdapter) (mChatList
                                    .getAdapter())).getCount() - 1);
                        }
                    });
                } else {
                    int lastViewedPosition = mChatList.getFirstVisiblePosition();

                    //get offset of first visible view
                    View v = mChatList.getChildAt(0);
                    int topOffset = (v == null) ? 0 : v.getTop();
                    mChatList.setSelectionFromTop(lastViewedPosition, topOffset);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        positionChange = false;
        try {
            NotificationManager notificationManager =
                    (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(Constants.CHAT_NOTIFICATION_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        refreshView();
    }
}
