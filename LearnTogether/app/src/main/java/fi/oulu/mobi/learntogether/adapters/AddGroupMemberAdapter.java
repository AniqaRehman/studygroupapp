package fi.oulu.mobi.learntogether.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.helpers.Buddy;

/**
 * Created by Sam on 4/22/2017.
 */

public class AddGroupMemberAdapter extends CursorAdapter {

    List<String> selectedBuddies = new ArrayList<>();

    public AddGroupMemberAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item_add_group, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        final String buddyUserId = cursor.getString(cursor.getColumnIndex
                (LearnTogetherContract.Buddies.USER_ID));

        ImageView pic = (ImageView) view.findViewById(R.id.buddy_picture);
        final String profilePicPath = cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.PROFILE_PICTURE));
        if(!profilePicPath.equals("")){
            File file = new File(profilePicPath);
            if(file.exists()){
                pic.setImageBitmap(BitmapFactory.decodeFile(profilePicPath));
            }
        }

        TextView memberNameText = (TextView) view.findViewById(R.id.group_list_item_name);
        memberNameText.setText(cursor.getString(cursor.getColumnIndex
                (LearnTogetherContract.Buddies.FIRST_NAME))+" "
                +cursor.getString(cursor.getColumnIndex(LearnTogetherContract.Buddies.LAST_NAME)));
        CheckBox selectedCheckBox = (CheckBox) view.findViewById(R.id.member_selection);
        selectedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    selectedBuddies.add(buddyUserId);
                }else{
                    selectedBuddies.remove(buddyUserId);
                }
            }
        });
    }

    public List<String> getSelectedBuddies() {
        return selectedBuddies;
    }
}
