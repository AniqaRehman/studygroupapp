package fi.oulu.mobi.learntogether.helpers;

import fi.oulu.mobi.learntogether.ui.UnreadMessages;

/**
 * Created by raniq on 4/19/2017.
 */

public class Constants {
    public static final String COGNITO_POOL_ID = "us-west-2:59426d25-7dc0-40c1-89ce-fde683f63ec8";
    public static final String COGNITO_ACOUNT_ID = "347118411249";
    public static final String COGNITO_ROLE_ARN = "arn:aws:iam::347118411249:role/Cognito_LearnTogetherUnauth_Role";

    public static final int NEW_WIDTH = 200;
    public static final int NEW_HEIGHT = 200;

    public static final String FOLDER_NAME = "Learn Together";
    public static final String FOLDER_DELIMITER = "/";
    public static final String PI_NAME = "PI.jpg";
    public static final String PROFILE_FOLDER_NAME = "Profile";

    //Fragments name
    public static final String ABOUT_US = "About US";
    public static final String ADD_GROUP = "Add GroupEntry";
    public static final String ADD_MEETING = "Add Meeting";
    public static final String CHAT_LIST = "Chat";
    public static final String COURSE_DETAIL = "Course Detail";
    public static final String COURSE_GROUP = "Course GroupEntry";
    public static final String HOME_FRAGMENT = "Learn Together";
    public static final String MEETING_DETAILS = "Meeting Detail";
    public static final String MEETING_LOCATION = "Meeting Location";
    public static final String MEETING = "Meeting";
    public static final String MY_BUDDIES = "My Buddies";
    public static final String MY_COURSES = "My Courses";
    public static final String MY_GROUP = "My Groups";
    public static final String PERSONAL_GROUP = "Personal GroupEntry";
    public static final String USER_PROFILE = "User Profile";
    public static final String ADD_COURSE = "Add Course";
    public static final String ADD_NOTE = "Add Note";

    //Extra keys
    public static final String MEETING_ID_EXTRA_KEY = "MeetingId";

    public static final String MEETING_IS_PREVIOUS_EXTRA = "isPrevious";
    public static final String GROUP_MEMBERS = "Group Members";
    public static final String REFRESH_MEMBERS = "refresh_members";
    public static final String NFC_BUDDY_MESSAGE = "nfc_buddy";

    public static final int CHAT_NOTIFICATION_ID = 1;
    public static final int BUDDY_NOTIFICATION_ID = 2;
    public static final int GROUP_NOTIFICATION_ID = 3;
    public static final int MEETING_NOTIFICATION_ID = 4;
    public static final String BUDDY_REQUESTS = "Buddy Requests";
    public static final String UPCOMING_MEETINGS = "Upcoming Meetings";
    public static final String UNREAD_MESSAGES = "Unread Messages";
    public static final int NOTE_NOTIFICATION_ID = 5;
    public static final String ALL_NOTIFICATION = "all_notification";
    public static final String CHAT_NOTIFICATION = "chat_notification";
    public static final String MEETING_NOTIFICATION = "meetings_notification";
    public static final String MEETING_NOTE_NOTIFICATION = "notes_notification";
    public static final String BUDDY_NOTIFICATION = "buddy_request_notification";

    public static enum GROUP_TYPES {PERSONAL, COURSE}

    public static enum GROUP_STATUS {CREATED, DELETED}

    public static enum MESSAGE_TYPE {TEXT, IMAGE, AUDIO, VIDEO}

    public static enum MESSAGE_STATUS {PENDING, DELIVERED, SENT, READ, UNREAD}

    public static enum NOTE_TYPE {PICTURE, TEXT}

    public static enum NOTE_DELIVERY_STATUS {DELIVERED, SHARED, CREATED}

    public static enum BUDDY_REQUEST_STATUS {
        DELIVERED, PENDING, ACCEPTED, REJECTED, RECEIVED
    }


}
