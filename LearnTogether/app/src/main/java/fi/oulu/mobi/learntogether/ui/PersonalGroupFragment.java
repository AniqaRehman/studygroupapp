package fi.oulu.mobi.learntogether.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;

import fi.oulu.mobi.learntogether.R;
import fi.oulu.mobi.learntogether.adapters.CourseAdapter;
import fi.oulu.mobi.learntogether.adapters.PersonalGroupAdapter;
import fi.oulu.mobi.learntogether.database.LearnTogetherContract;
import fi.oulu.mobi.learntogether.database.SQLiteManager;
import fi.oulu.mobi.learntogether.helpers.Group;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class PersonalGroupFragment extends Fragment {

    private String groupId;
    private static ListView mpersonalGroupList;
    static LearnTogetherContract ltDb;
    private PersonalGroupAdapter mpersonalGroupAdapter;
    static Context context;

    public PersonalGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_personal_group, container, false);
        setHasOptionsMenu(false);
        FloatingActionButton addCourses = (FloatingActionButton) rootView.findViewById(R.id.addPersonalGroup);
        addCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent = new Intent(getActivity(), AddGroupActivity.class);
                startActivity(newIntent);
            }
        });

        ltDb = new LearnTogetherContract(getContext());
        context = getContext();

        fi.oulu.mobi.learntogether.helpers.Group mygroupObject = new fi.oulu.mobi.learntogether.helpers.Group();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        mpersonalGroupList = (ListView) rootView.findViewById(R.id.personal_group_list);

        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static Handler UIHandler;

    static {
        UIHandler = new Handler(Looper.getMainLooper());
    }

    public static void runOnUI(Runnable runnable) {
        UIHandler.post(runnable);
    }

    public static void refreshGroupList() {
        if (mpersonalGroupList != null) {
            ltDb = new LearnTogetherContract(context);
            mpersonalGroupList.setAdapter(new PersonalGroupAdapter(context, ltDb.getPersonalGroups()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        StateMachine.setCurrentState(StateMachine.States.GROUPS);
        mpersonalGroupAdapter = new PersonalGroupAdapter(getContext(), ltDb.getPersonalGroups());
        mpersonalGroupList.setAdapter(mpersonalGroupAdapter);
    }


}
